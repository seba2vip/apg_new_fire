﻿using UnityEngine;
using System.Collections;

public class SpikeSpeedController : MonoBehaviour {

	private float Speed = 6.5f;
	private Rigidbody rigidBody;
	private bool isMoving = true;

	void Start(){
		rigidBody = transform.GetComponent<Rigidbody>();
        rigidBody.velocity = Vector3.zero;

    }

	void FixedUpdate() {
		if (isMoving) {
			Speed = SpeedController.Speed;
			if (rigidBody == null) {
				rigidBody = transform.GetComponent<Rigidbody> ();
			}
			rigidBody.velocity = Vector3.down * (Speed);
		} else {
			rigidBody.velocity = Vector3.zero;
		}
        //if (transform.position.y < -5)
            //Destroy(transform.gameObject);
	}

	void OnEnable() {
		Events.GameOverEvent += Stop;
	}

	//stop the spikes
	public void Stop(){
		isMoving = false;
	}

	void OnDisable() {
		Events.GameOverEvent -= Stop;
	}

}
