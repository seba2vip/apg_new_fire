﻿using UnityEngine;
using System.Collections;

public class ButtonSoundController : MonoBehaviour {

	public void PlaySound() {
		if (User.Instance.volume == true) {
			GetComponent<AudioSource> ().Play ();
		}
	}
}
