﻿using UnityEngine;
using System.Collections;

public class SpeedController : MonoBehaviour {

	public static float Speed = 5.5f;
		
	// Update is called once per frame
	void FixedUpdate () {
		Speed += Time.deltaTime / 7;
	}

	void OnEnable() {
		Events.GameOverEvent += OnGameOver;
	}

	public void OnGameOver(){
		Speed = 5.5f;
	}

	void OnDisable() {
		Events.GameOverEvent -= OnGameOver;
	}

}
