﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameManager_splitter : MonoBehaviour {
    public GameObject start;
    public GameObject gameOverG;
    //public GameObject secondChance;
    public Text time;
    float timeC;
    bool end;
    bool once;
    static int counterGameToFullAD=0;
    public int Score{get {return (int)score;}}
	public bool gameStart = true;

	private static float score;
	public bool gameOver;
    public Text bestScore;
    public Text currentluScore;

	void Start () {
        once = true;
        //counterGameToFullAD = 0;

        Events.GameOverEvent += GameOver;
        if(score>100)
        {
            start.SetActive(true);
            gameOverG.SetActive(false);
            //secondChance.SetActive(false);
            score = 0;
        }
        else
        {
            start.SetActive(false);
            gameOverG.SetActive(false);
            //secondChance.SetActive(false);
        }
        timeC = 5;
        end = false;
        if (!ADManager.isPremium)
            ADManager.instance.CreateInterstitial();
    }
    void Update()
    {
        if (end)
        {
            //secondChance.SetActive(true);
            //time.text = timeC.ToString();
            //timeC -= Time.deltaTime;
            //if (timeC < 0)
            {
                //   secondChance.SetActive(false);

                User.Instance.countGameSplitter++;
                if (User.Instance.countGameSplitter < 15) //reklamy
                {
                    Debug.Log("reklamy 15:" + User.Instance.countGameSplitter + " count:" + counterGameToFullAD);
                    counterGameToFullAD++;
                    User.Instance.saveUser();
                    if (!ADManager.isPremium)
                    {
                        if (counterGameToFullAD >= 3)
                        {
                            try
                            {
                                ADManager.instance.Display_InterstitialAD();
                                counterGameToFullAD = 0;

                            }
                            catch (Exception e) { Debug.Log(e); }

                        }
                    }
                }
                else
                {
                    Debug.Log("reklamy ");
                    try
                    {
                        if (!ADManager.isPremium) { 
                            ADManager.instance.Display_InterstitialAD();
                       
                        }
                    }
                    catch (Exception e) { Debug.Log(e); }

                }
            }
                if (score > User.Instance.hightScoreSplitter){
                    User.Instance.hightScoreSplitter = (int)score;
                    User.Instance.saveUser();
                }
                gameOverG.SetActive(true);
                bestScore.text = "Best Score:\n"+ User.Instance.hightScoreSplitter.ToString(); 
                currentluScore.text = "Score:\n" + score.ToString();
                end = false;
                score = 0;
               // timeC = 5;
            
        }
        if(gameOver && once)
        {
            end = true;
            once = false;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }
    }
        private void GameOver(){
		gameOver = true;
	}

	void OnDisable(){
		Events.GameOverEvent -= GameOver;
	}

	void OnTriggerExit(Collider other){
		if(other.tag.Equals("SpikeBoth")) {
			score += 0.5f * User.Instance.bonusPoint;
		} else if(other.tag.Equals("SpikeOther")){
			score+= 1 * User.Instance.bonusPoint;
		}
		if (score > PlayerPrefs.GetInt ("highscore")) {
			PlayerPrefs.SetInt ("highscore", (int)score);
		}
	}
    public void Exit()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void Play()
    {
        SceneManager.LoadScene("Game_Splitter");
    }
    public void SecondChance()
    {
        SceneManager.LoadScene("Game_Splitter");

    }
}
