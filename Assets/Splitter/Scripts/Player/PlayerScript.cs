﻿using UnityEngine;
using System.Collections;
public class PlayerScript : MonoBehaviour {

	public ParticleSystem sparksTop;
	public ParticleSystem sparksBottom;
	public AudioClip sawClip;

	private bool isPlaying = false;
	private AudioSource source;
    private void Start()
    {
        sparksTop.Stop();
        sparksBottom.Stop();
    }
    void OnEnable() {
		Events.GameOverEvent += OnGameOver;
		source = GetComponent<AudioSource> ();
		source.loop = true;
	}

	void OnDisable() {
		Events.GameOverEvent -= OnGameOver;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag.Equals("Wall")){
      		sparksTop.Play();
      		sparksBottom.Play();
      		isPlaying = true;

			//Play the saw clip
			if (User.Instance.volume == true) {
				source.PlayOneShot (sawClip, 0.3f);
			}
        }
    }
    
    void OnTriggerExit(Collider other)
    {
    	if(isPlaying && other.transform.tag.Equals("Wall")){
    		sparksTop.Stop();
    		sparksBottom.Stop();
    		isPlaying = false;

			source.Stop ();
    	}
    }

	void OnGameOver() {
		if (isPlaying) {
			sparksTop.Stop ();
			sparksBottom.Stop ();
			isPlaying = false;
		}
	}

}
