﻿using UnityEngine;

[RequireComponent(typeof (InputManager))]
public class PlayerManager : MonoBehaviour {

	private InputManager input;
	private bool gameOver = false;

	public PlayerController player;
    private void Awake()
    {
        User.Instance.loadUser() ;
    }
    void Start () {
	  input = GetComponent<InputManager>();
	  Events.GameOverEvent += PlayerHit;
	}
	
	void Update () {
		if(!gameOver && player!=null){
			player.Move(input.MappedInput);
		}
	}

	void OnDisable(){
		Events.GameOverEvent -= PlayerHit;
	}

	private void PlayerHit(){
		gameOver = true;
	}

	
}

