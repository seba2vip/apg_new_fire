﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Tuple<T1, T2>
{
	public T1 First { get; private set; }
	public T2 Second { get; private set; }
	internal Tuple(T1 first, T2 second)
	{
		First = first;
		Second = second;
	}
}

public class TileObject
{
	public GameObject tile;
	public int x, y;
}
public class GameManager2048 : MonoBehaviour {
    //public Transform lostscreen;
    //public Transform winscreen;
    public Text ModeButtonText;
    public GameObject Grid_x5;
    public Transform Camera;
    public GameObject menuPanel;
    //public Text textMenuPanel;
	public Text scoreLabel;
	public Text scoreBest;
	int score, bestscore, originalbest;
	public GameObject Tile;
    public bool keep = false;
	List<Tuple<int,int>> free = new List<Tuple<int,int>>();
    public int sizeBoard;// = 4;
    int[,] grid;// = new int[4, 4]; //znamia na 5x5 
    int[,] auxgrid;// = new int[4, 4];
	Queue<TileObject> tilegrid = new Queue<TileObject>();
	bool win, lose;
    bool checking;// = false;
    string str;//= "";
	Animator anim;
    float nextMove, timeMove;

    // Use this for initialization
    int transformXForMiniGame;
    private void Awake()
    {
        User.Instance.loadUser();
        if (User.Instance.mode2048 == User.Mode2048.x4)
        {
            sizeBoard = 4;
            grid = new int[4, 4];
            auxgrid = new int[4, 4];
            Grid_x5.SetActive(false);
            ModeButtonText.text = "Mode 5x5";
            Camera.position = new Vector3(1.5f, -0.5f, -8.7f);
        }
        else
        {
            sizeBoard = 5;
            grid = new int[5, 5];
            auxgrid = new int[5, 5];
            Grid_x5.SetActive(true);
            ModeButtonText.text = "Mode 4x4";
            Camera.position = new Vector3(2f, -0.5f, -10f);
        }
    }
    void Start () {

        transformXForMiniGame = 0;
        nextMove = 0 ;
        timeMove = 2;
        if (SceneManager.GetActiveScene().name == "MainMenu")
            transformXForMiniGame = 30;
        //textMenuPanel.text = "";
        menuPanel.SetActive(false);
        keep = false;
        checking = false;
        str = "";
    //bestscore = PlayerPrefs.GetInt("bestScore", 0); wyjebalem
    originalbest = bestscore;
		win = lose = false;
		for(int i = 0; i<sizeBoard; i++){
			for(int j = 0; j<sizeBoard; j++){
				grid[i,j] = 0;
			}
		}
//		grid[0,0] = 8;
//		grid[1,0] = 64;
//		grid[2,0] = 512;
//		grid[3,0] = 256;
//		grid[3,1] = 1024;
//		grid[3,2] = 2048;
//		grid[3,3] = 4096;
//		grid[2,2] = 8192;
		addRandomTile();
		addRandomTile();
		UpdateTiles();
		checking = false;
		score = 0;
		scoreLabel.text = "Score: " + score.ToString();
        if(User.Instance.mode2048 == User.Mode2048.x4)
            scoreBest.text = "Best: " + User.Instance.hightScore2048_x4.ToString();
        else
            scoreBest.text = "Best: " + User.Instance.hightScore2048_x5.ToString();
        if (!ADManager.isPremium)         //reklamy
            ADManager.instance.CreateInterstitial();
    }
	// Update is called once per frame
	void Update () {
        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            if ((!win || keep) && !lose)
            {
                Swipe();
                //if(Input.GetKeyDown(KeyCode.Space)){
                //	printStatus();
                //}
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    makeMoveUp();
                    makeCheck();
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    makeMoveDown();
                    makeCheck();
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    makeMoveRight();
                    makeCheck();
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    makeMoveLeft();
                    makeCheck();
                }
            }
        }
        else
        {
            if (Time.time > nextMove)
            {
                nextMove = Time.time + timeMove;
                moveAutoMiniGame();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
    void moveAutoMiniGame()
    {
        int dir = Random.Range(0, sizeBoard);
        switch (dir)
        {
            case 0:
                makeMoveUp();
                makeCheck();
                break;
            case 1:
                makeMoveDown();
                makeCheck();
                break;
            case 2:
                makeMoveRight();
                makeCheck();
                break;
            case 3:
                makeMoveLeft();
                makeCheck();
                break;

        }
    }
	void SavePrevGrid(){
		for(int i = 0; i<sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				auxgrid[i,j] = grid[i,j];
			}
		}
	}
	bool checkchanged(){
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(auxgrid[i,j] != grid[i,j])
					return true;
			}
		}
		return false;
	}
	void makeCheck(){
        if (User.Instance.mode2048 == User.Mode2048.x4)
        {
            if (score > User.Instance.hightScore2048_x4)
            {
                User.Instance.hightScore2048_x4 = score;
                User.Instance.saveUser();
            }

            scoreBest.text = "Best: " + User.Instance.hightScore2048_x4.ToString();
        }
        else
        {
            if (score > User.Instance.hightScore2048_x5)
            {
                User.Instance.hightScore2048_x5 = score;
                User.Instance.saveUser();
            }

            scoreBest.text = "Best: " + User.Instance.hightScore2048_x5.ToString();
        }
            

		scoreLabel.text = "Score: " + score.ToString();
		
		checking = true;
		//if(!keep)checkWin();
		if(!win || keep){
			if(checkchanged())
				addRandomTile();
			printgrid();
			checkLose();
		}
		UpdateTiles();
		checking = false;
	}
	void makeMoveUp(){
		SavePrevGrid();
		pileUp();
		for(int i = 0; i<sizeBoard-1; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(grid[i,j] == grid[i+1,j]){
					grid[i,j] *= 2;
					grid[i+1,j] = 0;
					if(!checking){
						score += grid[i,j] * User.Instance.bonusPoint;
						DeleteTile(i+1,j);
						AminateTile(i,j);
					}
					//if(!checking)tilegrid[i,j].GetComponent<TileController>().Fusion();
				}
			}
		}
		pileUp();
	}
	void pileUp(){
		Queue<int> aux = new Queue<int>();
		Queue<TileObject> aux2 = new Queue<TileObject>();
		TileObject t;
		for(int j = 0; j< sizeBoard; j++){
			aux.Clear();
			aux2.Clear();
			for(int i = 0; i< sizeBoard; i++){
				if(grid[i,j] != 0){
					aux.Enqueue(grid[i,j]);
					if(!checking){
						t = FindTile(i,j);
						if(t!=null)
							aux2.Enqueue(t);
					}
				}
			}
			for(int i = 0; i< sizeBoard; i++){
				grid[i,j]=0;
				if(aux.Count>0){
					grid[i,j] = aux.Dequeue();
					if(!checking){
						t = aux2.Dequeue();
						MoveTile(i, j, t);
					}
				}
			}
		}
	}

	void makeMoveDown(){
		SavePrevGrid();
		pileDown();
		for(int i = sizeBoard-1; i>0; i--){
			for(int j = 0; j< sizeBoard; j++){
				if(grid[i,j] == grid[i-1,j]){
					grid[i,j] *= 2;
					grid[i-1,j] = 0;
					if(!checking){
						score += grid[i,j] * User.Instance.bonusPoint;
						DeleteTile(i-1,j);
						AminateTile(i,j);
					}
					//if(!checking) tilegrid[i,j].GetComponent<TileController>().Fusion();
				}
			}
		}
		pileDown();
	}
	void pileDown(){
		Queue<int> aux = new Queue<int>();
		Queue<TileObject> aux2 = new Queue<TileObject>();
		TileObject t;
		for(int j = 0; j< sizeBoard; j++){
			aux.Clear();
			aux2.Clear();
			for(int i = sizeBoard-1; i>=0; i--){
				if(grid[i,j] != 0){
					aux.Enqueue(grid[i,j]);
					if(!checking){
						t = FindTile(i,j);
						if(t!=null)
							aux2.Enqueue(t);
					}
				}
			}
			for(int i = sizeBoard-1; i>=0; i--){
				grid[i,j]=0;
				if(aux.Count>0){
					grid[i,j] = aux.Dequeue();
					if(!checking){
						t = aux2.Dequeue();
						MoveTile(i, j, t);
					}
				}
			}
		}
	}

	void makeMoveRight(){
		SavePrevGrid();
		pileRight();
		for(int j = sizeBoard-1; j>0; j--){
			for(int i = 0; i< sizeBoard; i++){
				if(grid[i,j] == grid[i,j-1]){
					grid[i,j] *= 2;
					grid[i,j-1] = 0;
					if(!checking){
						score += grid[i,j] * User.Instance.bonusPoint;
						DeleteTile(i,j-1);
						AminateTile(i,j);
					}
					//if(!checking) tilegrid[i,j].GetComponent<TileController>().Fusion();
				}
			}
		}
		pileRight();
	}
	void pileRight(){
		Queue<int> aux = new Queue<int>();
		Queue<TileObject> aux2 = new Queue<TileObject>();
		TileObject t;
		for(int i = 0; i< sizeBoard; i++){
			aux.Clear();
			aux2.Clear();
			for(int j = sizeBoard-1; j>=0; j--){
				if(grid[i,j] != 0){
					aux.Enqueue(grid[i,j]);
					if(!checking){
						t = FindTile(i,j);
						if(t!=null)
							aux2.Enqueue(t);
					}
				}
			}
			for(int j = sizeBoard-1; j>=0; j--){
				grid[i,j]=0;
				if(aux.Count>0){
					grid[i,j] = aux.Dequeue();
					if(!checking){
						t = aux2.Dequeue();
						MoveTile(i, j, t);
					}
				}
			}
		}
	}
	void makeMoveLeft(){
		SavePrevGrid();
		pileLeft();
		for(int j = 0; j< sizeBoard-1; j++){
			for(int i = 0; i< sizeBoard; i++){
				if(grid[i,j] == grid[i,j+1]){
					grid[i,j] *= 2;
					grid[i,j+1] = 0;
					if(!checking){
						score += grid[i,j] * User.Instance.bonusPoint;
						DeleteTile(i,j+1);
						AminateTile(i,j);
					}
					//if(!checking) tilegrid[i,j].GetComponent<TileController>().Fusion();
				}
			}
		}
		pileLeft();
	}
	void pileLeft(){
		Queue<int> aux = new Queue<int>();
		Queue<TileObject> aux2 = new Queue<TileObject>();
		TileObject t;
		for(int i = 0; i< sizeBoard; i++){
			aux.Clear();
			aux2.Clear();
			for(int j = 0; j< sizeBoard; j++){
				if(grid[i,j] != 0){
					aux.Enqueue(grid[i,j]);
					if(!checking){
						t = FindTile(i,j);
						if(t!=null)
							aux2.Enqueue(t);
					}
				}
			}
			for(int j = 0; j< sizeBoard; j++){
				grid[i,j]=0;
				if(aux.Count>0){
					grid[i,j] = aux.Dequeue();
					if(!checking){
						t = aux2.Dequeue();
						MoveTile(i, j, t);
					}
				}
			}
		}
	}
	void checkWin(){
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(grid[i,j] == 2048){
					win = true;
                    //Invoke("goWin", 0.5f);
                    showMenu("WIN!");
					Debug.Log("WIN!!");
                    
                }
			}
		}
	}

	void freeCells(){
		free.Clear();
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(grid[i,j] == 0)
					free.Add(new Tuple<int, int>(i,j));
			}
		}
	}
	void addRandomTile(){
		freeCells();
		if(free.Count==0){
			return;
		}
		int r = Random.Range(0, free.Count);
		int i, j;
		i = free[r].First;
		j = free[r].Second;
		if(Random.value < 0.9f){
			grid[i,j] = 2;
		}else{
			grid[i,j] = 4;
		}
		AddTile(i,j);
	}

	void checkLose(){
		int[,] auxgrid = new int[sizeBoard, sizeBoard];
		bool done = false;
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				auxgrid[i,j] = grid[i,j];
			}
		}
		makeMoveUp();
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(auxgrid[i,j] != grid[i,j]){
					done = true;
					goto EndIt;
				}
			}
		}
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				grid[i,j] = auxgrid[i,j];
			}
		}
		makeMoveDown();
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(auxgrid[i,j] != grid[i,j]){
					done = true;
					goto EndIt;
				}
			}
		}
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				grid[i,j] = auxgrid[i,j];
			}
		}
		makeMoveRight();
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(auxgrid[i,j] != grid[i,j]){
					done = true;
					goto EndIt;
				}
			}
		}
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				grid[i,j] = auxgrid[i,j];
			}
		}
		makeMoveLeft();
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				if(auxgrid[i,j] != grid[i,j]){
					done = true;
					goto EndIt;
				}
			}
		}

	EndIt:
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				grid[i,j] = auxgrid[i,j];
			}
		}
		if(!done){
			lose = true;
            //Invoke("goLost", 1.0f);
            showMenu("LOST!");
			Debug.Log("LOST!");
            if (!ADManager.isPremium)           //reklamy
            {
                ADManager.instance.Display_InterstitialAD();
            }
        }
	}

	void printgrid(){
		str = "";
		for(int i = 0; i< sizeBoard; i++){
			for(int j = 0; j< sizeBoard; j++){
				str+= "" + grid[i,j] + ",\t\t";
			}
			str+="\n\n";
		}
		//Debug.Log(str);
	}
	void UpdateTiles(){
		foreach(TileObject tile in tilegrid){
			int val = grid[tile.x, tile.y];
			tile.tile.GetComponent<TileController>().SetValue(val);
		}
	}
	void AddTile(int i, int j){
		TileObject _tile = new TileObject();
		_tile.tile = (GameObject)Instantiate(Tile);
		_tile.tile.transform.position = new Vector3(j+ transformXForMiniGame, -i, 0);
		_tile.tile.transform.parent = transform;
		_tile.x = i;
		_tile.y = j;
		tilegrid.Enqueue(_tile);
		//Debug.Log("nuevo tile en: " +_tile.x.ToString() + " , " + _tile.y.ToString() + " = " + grid[i,j].ToString());
	}
	void MoveTile(int i, int j, TileObject t){
		if(t!=null){
			t.tile.GetComponent<translateScript>().MoveTo(new Vector3(j+transformXForMiniGame, -i, 0));
			t.x=i;
			t.y=j;
			//t.tile.transform.position = new Vector3(j, -i, 0);
			tilegrid.Enqueue(t);
		}
	}
	TileObject FindTile(int i, int j){
		TileObject ret;
		for(int k=0; k<tilegrid.Count; k++){
			ret = tilegrid.Dequeue();
			if(ret.x==i && ret.y==j)
				return ret;
			tilegrid.Enqueue(ret);
		}
		return null;
	}
	void DeleteTile(int i, int j){
		TileObject _tile;
		for(int k=0; k<tilegrid.Count; k++){
			_tile = tilegrid.Dequeue();
			if(_tile.x==i && _tile.y==j){
				Destroy(_tile.tile);
				return;
			}
			tilegrid.Enqueue(_tile);
		}
	}
	void printStatus(){
		foreach(TileObject tile in tilegrid){
			Debug.Log("hay un tile en: " + tile.x.ToString() + ", " + tile.y.ToString() + " = " + grid[tile.x, tile.y].ToString());
		}
	}
	void AminateTile(int i, int j){
		foreach(TileObject tile in tilegrid){
			if(tile.x==i && tile.y==j)
				tile.tile.GetComponent<TileController>().Fusion();
		}
	}
    public void ButtonExit()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void ButtonNewGame()
    {
        SceneManager.LoadScene("Game_2048");
    }
    void showMenu(string text)
    {
        menuPanel.SetActive(true);
        //textMenuPanel.text = text;
    }

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    public void Swipe()
    {
        /*if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                //save began touch 2d point
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }
            if (t.phase == TouchPhase.Ended)
            {
                //save ended touch 2d point
                secondPressPos = new Vector2(t.position.x, t.position.y);

                //create vector from the two points
                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                //normalize the 2d vector
                currentSwipe.Normalize();

                //swipe upwards
                if (currentSwipe.y > 0 &&  currentSwipe.x > -0.5f &&  currentSwipe.x < 0.5f)
             {
                    makeMoveUp();
                    makeCheck();
                    Debug.Log("up swipe");
                }
                //swipe down
                if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
             {
                    makeMoveDown();
                    makeCheck();
                    Debug.Log("down swipe");
                }
                //swipe left
                if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
             {
                    makeMoveLeft();
                    makeCheck();
                    Debug.Log("left swipe");
                }
                //swipe right
                if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
             {
                    makeMoveRight();
                    makeCheck();
                    Debug.Log("right swipe");
                }
            }
        }*/
        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            //save ended touch 2d point
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();

            //swipe upwards
            if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                makeMoveUp();
                makeCheck();
                Debug.Log("up swipe");
            }
            //swipe down
            if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                makeMoveDown();
                makeCheck();
                Debug.Log("down swipe");
            }
            //swipe left
            if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                makeMoveLeft();
                makeCheck();
                Debug.Log("left swipe");
            }
            //swipe right
            if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                makeMoveRight();
                makeCheck();
                Debug.Log("right swipe");
            }
        }
 
    }
    public void ModeButton()
    {
        if (User.Instance.mode2048 == User.Mode2048.x4)
            User.Instance.mode2048 = User.Mode2048.x5;
        else
            User.Instance.mode2048 = User.Mode2048.x4;


        User.Instance.saveUser();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
    //void goLost(){
    //	Instantiate(lostscreen);
    //	//if(bestscore>originalbest)
    //		//PlayerPrefs.SetInt("bestScore", bestscore);
    //}
    //void goWin(){
    //	Instantiate(winscreen);
    //	//if(bestscore>originalbest)
    //		//PlayerPrefs.SetInt("bestScore", bestscore);
    //}
}
