﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
//using Fabric.Answers;


[System.Serializable]
public class GameManager : MonoBehaviour {
    //User_Circuit.User_Circuit user_Circuit;
    private static int counterGameToFullAD=0;
    public static bool canClick = true;
	public GameObject winPanel;
//	public FullscreenAd fullscreenAd;
	public GameObject[] piecePrefabs;
	[SerializeField]
	public static List<Puzzle> puzzles = new List<Puzzle>();
	public List<Color> colors = new List<Color> ();
	[System.Serializable]
	public class Puzzle
	{
		[SerializeField]
		public int winValue;
		[SerializeField]
		public int curValue;
		[SerializeField]
		public int width;
		[SerializeField]
		public int height;

		[SerializeField]
		public Piece[,] pieces;
	}

    //public static int index = 1;
    private void Awake()
    {
        User.Instance.loadUser();
    }
    public Puzzle puzzle;

	/*public void loadUser(){
		string s = Application.persistentDataPath + "/usrs";
		if (File.Exists (s)) {
			Debug.Log ("Gowno2");
			try {
				BinaryFormatter bf2 = new BinaryFormatter ();
				FileStream file2 = File.Open (s, FileMode.Open);
				Debug.Log ("Gowno3");
                user_Circuit = (User_Circuit.User_Circuit)bf2.Deserialize (file2);
			} catch (Exception e) {
                user_Circuit = new User_Circuit.User_Circuit();
				Debug.Log (e.Message);
			}
		} else {
            user_Circuit = new User_Circuit.User_Circuit();
		}
	}*/

	/*public void saveUser(){
		string s = Application.persistentDataPath + "/usrs";
		try
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(s);
			bf.Serialize(file, user_Circuit);
			file.Close();
		}
		catch (System.Exception e) {
			//saveUser ();
			Debug.Log("saving error " + e.Message);
		}
	}*/
	public static Color hexToColor(string hex)
	{
		hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
		hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		//Only use alpha if the string has enough characters
		if(hex.Length == 8){
			a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r,g,b,a);
	}

	void Start () {
        if (!ADManager.isPremium)                 //reklamy
            ADManager.instance.CreateInterstitial();
        if (colors.Count == 0){
            //FadeOut (0); 
            Debug.Log("start circuit");
			colors.Add(hexToColor("767373AF"));//szary
		colors.Add(hexToColor("004BDDFF"));//niebieski
		colors.Add(hexToColor("6122E7FF"));//fiolet
		colors.Add(hexToColor("BA3019FF"));//czerwony
		colors.Add(hexToColor("3DAB3EFF"));//zielony
		colors.Add(hexToColor("A53DABFF"));//rózowy
		colors.Add(hexToColor("379898FF"));//turkusowy
		colors.Add(hexToColor("A6AE17F5"));//zolty
		}
		if (true) {
			/*puzzle = new Puzzle ();
			puzzle.width = index-1;
			puzzle.height = index;
			
			*/
			if (ll == null || ll.Count == 0) {
				//string ss = "Assets/Circuit/Assets/Resources/lvls2.txt";
				TextAsset ts = Resources.Load ("lvls7") as  TextAsset;
                Debug.Log("lvl3: " + ts);
				MemoryStream sr = new MemoryStream (ts.bytes);
				if (sr != null) {
					Debug.Log ("Gowno2");
					try {
						BinaryFormatter bf2 = new BinaryFormatter ();
						//	FileStream file2 = File.Open(ss, FileMode.Open);
						Debug.Log ("Gowno3");
						//	bf2.d
						ll = (List<Level>)bf2.Deserialize (sr);
                        Debug.Log(" llcount:" + ll.Count);
                    } catch (Exception e) {
						Debug.Log (e.Message);
					}
				}
                
                canClick = true;
				if (ll.Count > 1) {
                    ll.RemoveAt(3);

                } else {
					ll = new List<Level>();
			/*generatePuzzles();

				BinaryFormatter bf = new BinaryFormatter();
				string s = Application.persistentDataPath + "/lvls7.bytes";
                    Debug.Log("sciezka: " + s);
				FileStream file = File.Create(s);
				bf.Serialize(file, ll);
				file.Close();
				if (ll == null || ll.Count == 0) {
					ll = null;
					if (File.Exists (s)) {
						Debug.Log ("Gowno2");
						try {
							BinaryFormatter bf2 = new BinaryFormatter ();
							FileStream file2 = File.Open (s, FileMode.Open);
							Debug.Log ("Gowno3");
							ll = (List<Level>)bf2.Deserialize (file2);
						} catch (Exception e) {
							Debug.Log (e.Message);
						}
					}
				}
					//puzzle = ll [user.currentLevel];
					//init ();

					/* if (puzzle.winValue == 0) {
				foreach (var piece in GameObject.FindGameObjectsWithTag("Pieces")) {
					Destroy (piece);
				}
					Start ();
				}*/
				}
			} else {
				canClick = true;
				//init ();
			}
		} 
	}

	public void init(){
		foreach (var piece in GameObject.FindGameObjectsWithTag("Pieces")) {
			Destroy (piece);
		}
        //Debug.Log("obecny level: " + User_Circuit.User_Circuit.currentLevel);
        //newLevel = true;
        //newLevelEnd = false;	
        /*if (User_Circuit.User_Circuit.currentLevel == 0) {
			puzzle.width = 2;
			puzzle.height = 2;
			GeneratePuzzle();
			//puzzle.winValue = GetWinValue ();

		}else
		if (User_Circuit.User_Circuit.currentLevel <= 2) {
			puzzle.width = 3;
			puzzle.height = 2;
			GeneratePuzzle ();
			//puzzle.winValue = GetWinValue ();
				
		} else {
            Debug.Log("currrlevel: " + User_Circuit.User_Circuit.currentLevel +" llcount:"+ ll.Count);

            puzzle = getPuzzle (ll [User_Circuit.User_Circuit.currentLevel]);
		}*/
        puzzle = getPuzzle(ll[User.Instance.levelCurcuit]);
        //puzzle = getPuzzle(ll[User_Circuit.User_Circuit.currentLevel]);
        Color c = colors [UnityEngine.Random.Range (0, colors.Count - 1)];
		Camera.main.transform.position = new Vector3 (puzzle.width / 2.0f - 0.5f, puzzle.height / 2.0f - 0.5f, -10);
		for (int h = 0; h < puzzle.height; h++) {
			for (int w = 0; w < puzzle.width; w++) {
				//GameObject go =  (GameObject) Instantiate (piecePrefabs [puzzle.pieces[w,h].type], new Vector3 (w, h, 0), Quaternion.identity);



				/*{
					go.GetComponent<Piece> ().RotatePiece ();
				}

				puzzle.pieces [w, h] = go.GetComponent<Piece> ();
				*/if (puzzle.pieces [w, h] != null) {
					SpriteGlow.SpriteGlow sg = puzzle.pieces [w, h].GetComponent<SpriteGlow.SpriteGlow> ();
					if (sg != null) {
						sg.GlowColor = c;
					}
				}
				//puzzle.pieces [w, h].type = valueSum;
			}
		}
		puzzle.winValue = GetWinValue();
		Shuffle ();
		puzzle.curValue = Sweep();
	}
    public void generatePuzzles()
    {
        Debug.Log("generatePuzzles");
        int counter = 1;
        while (true)
        {
            puzzle = new Puzzle();
            if (counter < 2)
            {
                puzzle.width = counter + 1;
                puzzle.height = 1;
            }
            else if (counter < 6)
            {
                puzzle.width = 3;
                puzzle.height = 2;
            }
            else if (counter < 8)
            {
                puzzle.width = 4;
                puzzle.height = 2;
            }
            else if (counter < 10)
            {
                puzzle.width = 3;
                puzzle.height = 3;
            }
            else if (counter < 15)
            {
                puzzle.width = 3;
                puzzle.height = 4;
            }
            else if (counter < 20)
            {
                puzzle.width = 4;
                puzzle.height = 4;
            }
            else if (counter < 25)
            {
                puzzle.width = 5;
                puzzle.height = 4;
            }
            else if (counter < 30)
            {
                puzzle.width = 5;
                puzzle.height = 5;
            }
            else if (counter < 35)
            {
                puzzle.width = 5;
                puzzle.height = 6;
            }
            else if (counter < 40)
            {
                puzzle.width = 6;
                puzzle.height = 6;
            }
            else if (counter < 50)
            {
                puzzle.width = 6;
                puzzle.height = 7;
            }
            else if (counter < 60)
            {
                puzzle.width = 6;
                puzzle.height = 8;
            }
            else if (counter < 70)
            {
                puzzle.width = 6;
                puzzle.height = 8;
            }
            else if (counter < 120)
            {
                puzzle.width = 7;
                puzzle.height = 8;
            }
            else if (counter < 130)
            {
                puzzle.width = 7;
                puzzle.height = 8;
            }
            else if (counter < 150)
            {
                puzzle.width = 7;
                puzzle.height = 9;
            }
            else if (counter < 160)
            {
                puzzle.width = 7;
                puzzle.height = 10;
            }
            else if (counter < 170)
            {
                puzzle.width = 7;
                puzzle.height = 11;
            }
            else if (counter < 200)
            {
                puzzle.width = 7;
                puzzle.height = 12;
            }
            else
            {
                break;
                return;
            }
            GeneratePuzzle();
            puzzle.winValue = GetWinValue();
            foreach (var piece in GameObject.FindGameObjectsWithTag("Pieces"))
            {
                Destroy(piece);
            }
            if (puzzle.winValue == 0)
            {
                foreach (var piece in GameObject.FindGameObjectsWithTag("Pieces"))
                {
                    Destroy(piece);
                }
                //Start ();
            }
            else
            {
                bool canAdd = true;
                for (int i = 0; i < puzzles.Count; i++)
                {
                    if (puzzles[i].pieces.Length == puzzle.pieces.Length)
                    {
                        bool isDiff = false;
                        foreach (Piece p in puzzles[i].pieces)
                        {
                            foreach (Piece pp in puzzle.pieces)
                            {
                                if (pp.type != p.type)
                                {
                                    isDiff = true;
                                }
                                else
                                {

                                }
                            }
                        }
                        if (isDiff == false)
                        {
                            canAdd = false;
                        }
                    }
                }

                if (canAdd)
                {
                    if (counter < 8)
                    {

                        foreach (Piece pp in puzzle.pieces)
                        {
                            if (pp.type == 0)
                            {
                            }
                        }
                    }
                    counter++;

                    //Shuffle ();
                    puzzle.curValue = Sweep();
                    puzzles.Add(puzzle);
                    string s = "";

                    Level l = new Level();
                    l.height = puzzle.height;
                    l.width = puzzle.width;
                    l.winValue = puzzle.winValue;
                    l.pieces = new int[l.width, l.height];
                    for (int h = 0; h < puzzle.height; h++)
                    {
                        for (int w = 0; w < puzzle.width; w++)
                        {
                            l.pieces[w, h] = puzzle.pieces[w, h].type;
                        }
                    }
                    ll.Add(l);
                    Debug.Log("ll.Add(l " + ll.Count);


                }
            }
        }
    }

    List<Level> ll = new List<Level> ();
	[System.Serializable]
	public class Level{
		[SerializeField]
		public int winValue;
		[SerializeField]
		public int width;
		[SerializeField]
		public int height;

		[SerializeField]
		public int[,] pieces;
	}

	public Puzzle getPuzzle(Level l){

		puzzle = new Puzzle ();
		puzzle.height = l.height;
		puzzle.width = l.width;
		puzzle.pieces = new Piece[l.width, l.height];
		for (int h = 0; h < puzzle.height; h++) {
			for (int w = 0; w < puzzle.width; w++) {
				GameObject go =  (GameObject) Instantiate (piecePrefabs [l.pieces [w, h]], new Vector3 (w, h, 0), Quaternion.identity);

				puzzle.pieces [w, h] = go.GetComponent<Piece> ();
					puzzle.pieces [w, h].type = l.pieces [w, h];
			}
		}
		puzzle.winValue = GetWinValue ();
		return puzzle;
	}

	Puzzle GeneratePuzzle()
	{
		puzzle.pieces = new Piece[puzzle.width, puzzle.height];
		int[] auxValues = { 0, 0, 0, 0 };

		for (int h = 0; h < puzzle.height; h++) {
			for (int w = 0; w < puzzle.width; w++) {
				//width restrictions
				if (w == 0)
					auxValues [3] = 0;
				else
					auxValues [3] = puzzle.pieces [w - 1, h].values [1];

				if (w == puzzle.width - 1)
					auxValues [1] = 0;
				else
					auxValues [1] = UnityEngine.Random.Range (0, 2);

				//heigth resctrictions
				if (h == 0)
					auxValues [2] = 0;
				else
					auxValues [2] = puzzle.pieces [w, h - 1].values [0];

				if (h == puzzle.height - 1)
					auxValues [0] = 0;
				else
					auxValues [0] = UnityEngine.Random.Range (0, 2);
				
				//tells us piece type
				int valueSum = auxValues [0] + auxValues [1] + auxValues [2] + auxValues [3];


				if (valueSum == 2 && auxValues [0] != auxValues [2])
					valueSum = 5;

				GameObject go =  (GameObject) Instantiate (piecePrefabs [valueSum], new Vector3 (w, h, 0), Quaternion.identity);

				while (go.GetComponent<Piece> ().values [0] != auxValues [0] ||
					go.GetComponent<Piece> ().values [1] != auxValues [1] ||
					go.GetComponent<Piece> ().values [2] != auxValues [2] ||
					go.GetComponent<Piece> ().values [3] != auxValues [3]) 

				{
					go.GetComponent<Piece> ().RotatePiece ();
				}

				puzzle.pieces [w, h] = go.GetComponent<Piece> ();
				puzzle.pieces [w, h].type = valueSum;

			}
		}

		return puzzle;

	}


	Vector2 CheckDimensions()
	{
		Vector2 aux = Vector2.zero;

		GameObject[] pieces = GameObject.FindGameObjectsWithTag ("Pieces");

		foreach (var p in pieces) {
			if (p.transform.position.x > aux.x)
				aux.x = p.transform.position.x;

			if (p.transform.position.y > aux.y)
				aux.y= p.transform.position.y;
		}

		aux.x++;
		aux.y++;

		return aux;
	}

	public int Sweep()
	{
		int value = 0;
		for (int h = 0; h < puzzle.height; h++) {
			for (int w = 0; w < puzzle.width; w++) {
				//compares top
				if (h != puzzle.height - 1) {
					if (puzzle.pieces [w, h].values [0] == 1 && puzzle.pieces [w, h + 1].values [2] == 1) {
						value++;
					}
				}
					
				//compare right
				if (w != puzzle.width - 1) {
					if (puzzle.pieces [w, h].values [1] == 1 && puzzle.pieces [w + 1, h].values [3] == 1) {
						value++;
					}
				}
			}
		}

		return value;

	}

	public int QuickSweep(int w,int h)
	{
		int value = 0;

		//compares top
		if(h!=puzzle.height-1)
		if (puzzle.pieces [w, h].values [0] == 1 && puzzle.pieces [w, h + 1].values [2] == 1)
			value++;

		//compare right
		if(w!=puzzle.width-1)
		if (puzzle.pieces [w, h].values [1] == 1 && puzzle.pieces [w + 1, h].values [3] == 1)
			value++;

		//compare left
		if (w != 0)
		if (puzzle.pieces [w, h].values [3] == 1 && puzzle.pieces [w - 1, h].values [1] == 1)
			value++;

		//compare bottom
		if (h != 0)
		if (puzzle.pieces [w, h].values [2] == 1 && puzzle.pieces [w, h-1].values [0] == 1)
			value++;

		return value;
	}

	public void Win()
	{
		Debug.Log ("Win");
       
        //Time.timeScale = 0;
        canClick = false;
		//StartCoroutine(LoadNextSceneCoroutine());
		//winPanel.GetComponents()
		//winPanel.SetActive (true);

	}
	private float speed = 1f;
	private IEnumerator LoadNextSceneCoroutine()    {
		//callOnce = true;
		CanvasGroup b = winPanel.GetComponent<CanvasGroup>();
			//c.GetComponents
		while(b.alpha < 1){
			b.alpha += Time.deltaTime * speed;
			yield return null;
		}
		//Application.LoadLevel("Scene2");
	}

	int GetWinValue()
	{
		int winValue = 0;
		foreach (var piece in puzzle.pieces) {
			foreach (var j in piece.values) {
				winValue += j;
			}
		}

		winValue /= 2;
		return winValue;
	}


	void Shuffle()
	{
		foreach (var piece in puzzle.pieces) {
			int k = UnityEngine.Random.Range (0, 4);
			for (int i = 0; i < k; i++) {
				piece.RotatePiece ();
			}
		}
	}
	private int adCounter =0;
    bool newLevel = true;
	bool newLevelEnd = false;	
	void Update () {
		if (canClick == false) {
			CanvasGroup b = winPanel.GetComponent<CanvasGroup> ();
			//c.GetComponents

			if (b.alpha < 1) {
				b.alpha += Time.deltaTime * speed/2;
				Camera.main.GetComponent<Kino.Bloom> ().Intensity += 0.05f;
				//	yield return null;
			}else{
				/*if (user_Circuit.currentLevel >= 15) 
				{
					if (user_Circuit.currentLevel == 15) {
						//fullscreenAd.showAd ();
					} else if (user_Circuit.currentLevel <= 50) {
						if (adCounter >= 5) {
							adCounter = 0;
							//if (fullscreenAd.showAd ()) {
								//fullscreenAd.loadAd ();
							//}
						} else {
							adCounter++;
						}
					}else if (user_Circuit.currentLevel <= 100)  {
						if (adCounter >= 4) {
							adCounter = 0;
							//if (fullscreenAd.showAd ()) {
								//fullscreenAd.loadAd ();
							//}
						} else {
							adCounter++;
						}
					}else   {
						if (adCounter >= 3) {
							adCounter = 0;
							//if (fullscreenAd.showAd ()) {
								//fullscreenAd.loadAd ();
							//}
						} else {
							adCounter++;
						}
					}
				}*/
                if (!ADManager.isPremium)           //reklamy
                {
                    if (User.Instance.levelCurcuit < 20)
                    {
                        counterGameToFullAD++;
                        if (counterGameToFullAD >= 3)
                        {
                            try
                            {
                                ADManager.instance.Display_InterstitialAD();
                                counterGameToFullAD = 0;
                               

                            }
                            catch (Exception e) { }
                        }
                    }
                    else
                    {
                        try
                        {
                            ADManager.instance.Display_InterstitialAD();
                            
                        }
                        catch (Exception e) { }
                    }
                    
                }
                NextLevel ();
			}
		} else {
			GameObject g = GameObject.Find ("TextLevel");
			UnityEngine.UI.Text t = g.GetComponent<UnityEngine.UI.Text> ();
            t.text = "#" + (User.Instance.levelCurcuit + 1);
            //t.text = "#" + (User_Circuit.User_Circuit.currentLevel+1);
            CanvasGroup b = g.GetComponent<CanvasGroup> ();
			if (newLevel) {
				if (b.alpha < 1) {
					b.alpha += Time.deltaTime * speed;
					//	yield return null;
				} else {
					newLevel = false;
				}
			} else {
				if (b.alpha > 0) {
					b.alpha -= Time.deltaTime * speed;
					//	yield return null;
				}else{
					if (newLevelEnd == false) {
						newLevelEnd = true;

						init ();
					}
				}
			}

		}
	}

	public void NextLevel(){
		CanvasGroup b = winPanel.GetComponent<CanvasGroup> ();

		Camera.main.GetComponent<Kino.Bloom> ().Intensity = 0.4f;
		if (canClick == false && b.alpha == 1) {
			b.alpha = 0;

            //if(User_Circuit.User_Circuit.currentLevel == User_Circuit.User_Circuit.unLockLevel)
            //User_Circuit.User_Circuit.unLockLevel++;
            User.Instance.levelCurcuit ++;
            User.Instance.levelUnlock ++;
           // User_Circuit.User_Circuit.currentLevel++;
            /*Answers.LogCustom(
				"Level"+ user_Circuit.currentLevel,
				customAttributes: new Dictionary<string, object> (){
					{"currentLevel", user_Circuit.currentLevel}
				}
			);*/
            //index++;
			foreach (var piece in GameObject.FindGameObjectsWithTag("Pieces")) {
				Destroy (piece);
			}
            
            User.Instance.saveUser();
			Start ();

			newLevel = true;
			newLevelEnd = false;	
		}
	}

	public void ShowMenu(){
		
	}
}
