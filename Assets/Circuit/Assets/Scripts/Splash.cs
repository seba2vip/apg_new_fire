﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

	// Use this for initialization
	void Start () {
		System.Threading.Thread.Sleep(
			(int)System.TimeSpan.FromSeconds(3).TotalMilliseconds);
		SceneManager.LoadScene ("Game_Speed Box");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
