﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Menu : MonoBehaviour {

    GameObject menuPanel;
   // public Image imageMusic;
    public AudioSource musicMute;
    User_Circuit.User_Circuit user_Circuit;
    public GameManager gameManager;
    bool menuP;
    //Sprite onMusic;
    //Sprite offMusic;
    // Use this for initialization
    void Start () {
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        user_Circuit = new User_Circuit.User_Circuit();
        menuPanel = GameObject.Find("menuPanel");
        menuPanel.SetActive(false);
        //onMusic = Resources.Load<Sprite>("music_on");
        //offMusic = Resources.Load<Sprite>("music_off");
        //imageMusic.sprite = onMusic;
        menuP = false;
        musicMute.mute = !User.Instance.volume;//false;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitButton();
        }
    }
    public void MenuButton()
    {
        menuP = !menuP;
        if (menuP)
        {
            menuPanel.SetActive(true);
        }
        else
        {
            menuPanel.SetActive(false);
        }
    }
    public void StarButton()
    {

    }
    /*public void MusicButton()
    {
        musicMute.mute = !musicMute.mute;
        if (!musicMute.mute)
        {
            imageMusic.sprite = onMusic;
        }
        else
        {
            imageMusic.sprite = offMusic;
        }
    }*/
    public void LeftButton()
    {
        if (User.Instance.levelCurcuit - 1 >= 0)
        {
            User.Instance.levelCurcuit -= 1;
            gameManager.init();
        }
    }
    public void RightButton()
    {
        Debug.Log("unLockLevel:" + User.Instance.levelUnlock + " cur lvl:"+ User.Instance.levelUnlock);
        if(User.Instance.levelUnlock >= User.Instance.levelCurcuit + 1)
        {
            User.Instance.levelCurcuit += 1;
            gameManager.init();
        }
        
    }
    public void ExitButton()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
