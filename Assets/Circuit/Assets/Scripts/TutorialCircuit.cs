﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCircuit : MonoBehaviour
{
    public GameObject tutorial;
    User_Circuit.User_Circuit user_Circuit;
    private void Start()
    {
        if (User.Instance.levelCurcuit == 0)
            tutorial.SetActive(true);
        else
            tutorial.SetActive(false);
    }

    public void buttonClose()
    {
        tutorial.SetActive(false);
    }
}
