﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public class Piece : MonoBehaviour {
	[SerializeField]
	public int type = -1;
	[SerializeField]
	public int[] values;
	[SerializeField]
	public static float speed = 0.3f;
	float realRotation;
	public GameManager gameManager;

	// Use this for initialization
	void Start () {
        gameManager = GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameManager> ();
	}

	// Update is called once per frame
	void Update () {
		if (transform.rotation.eulerAngles.z != realRotation) {
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.Euler (0, 0, realRotation), speed);
		}	
	}
		

	void OnMouseDown()
	{
		if (!GameManager.canClick) {
			return;
		}

		int difference = -gameManager.QuickSweep((int)transform.position.x,(int)transform.position.y);

		RotatePiece ();

		difference += gameManager.QuickSweep((int)transform.position.x,(int)transform.position.y);
        gameManager.puzzle.curValue += difference;

		if (gameManager.puzzle.curValue == gameManager.puzzle.winValue)
            gameManager.Win ();
	}

	public void RotatePiece()
	{
		
			realRotation += 90;
			if (realRotation == 360)
				realRotation = 0;

			RotateValues ();

	}


	public void RotateValues()
	{

		int aux = values [0];

		for (int i = 0; i < values.Length-1; i++) {
			values [i] = values [i + 1];
		}
		values [3] = aux;
	}
}
