﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ActivButton : MonoBehaviour
{
    public Button bt;
    private int curentIndex;
    public VideoPlayer video;
    //public Image image;
    //public GameObject box;
    void Start()
    {
        char s = transform.name[transform.name.Length - 1];
        curentIndex = Convert.ToInt32(new string(s, 1));
        //video.time = 1;
        video.Play();
        //image.mainTexture = video.texture;
        video.Pause();
        if (MoveBoxGameMenu.indexGameBox == curentIndex)
        {
            bt.interactable = true;
            video.Play();
            //video.playOnAwake = true;
        }

        // bt = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
       //Debug.Log(transform.name+"pozycja : "+transform.position.y);
        if(MoveBoxGameMenu.indexGameBox == curentIndex && bt.interactable == false)
        {
           // Debug.Log(transform.name + " :zmian na true activ "+curentIndex);
            bt.interactable = true;
            video.Play();
        }
        else if(MoveBoxGameMenu.indexGameBox != curentIndex && bt.interactable == true)
        {
            //Debug.Log(transform.name + " :zmian na false activ " + curentIndex);
                bt.interactable = false;
            video.Pause();

        }

        }
    }
