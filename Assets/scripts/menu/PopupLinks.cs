﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupLinks : MonoBehaviour
{
    public void Button_0()
    {
        Application.OpenURL("https://support.google.com/admob/answer/6128543?hl=en");
    }
    public void Button_1()
    {
        Application.OpenURL("https://www.adcolony.com/privacy-policy");
    }
    public void Button_2()
    {
        Application.OpenURL("https://unity3d.com/legal/privacy-policy");
    }
    public void Button_3()
    {
        Application.OpenURL("https://www.applovin.com/privacy");
    }
    public void Button_4()
    {
        Application.OpenURL("https://vungle.com/privacy");
    }
    public void Button_5()
    {
        Application.OpenURL("https://firebase.google.com/policies/analytics");
    }
    public void Button_6()
    {
        Application.OpenURL("http://apgsoftware.pl/privacy_policy.html");
    }

}
