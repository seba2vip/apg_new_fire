﻿using System.Collections;
using System.Collections.Generic;
using GoogleMobileAdsMediationTestSuite.Api;
using UnityEngine;
using UnityEngine.UI;

public class MoveBoxGameMenu : MonoBehaviour {

    private Vector3 tempPos;
    private Vector3 targetPos;
    private Vector3 tempY;
    static GameObject BoxGame;
    public static int indexGameBox = 0;
    //private Color originalColor;
    //private Color selectGameColor= Color.red;

    GameObject[] strzalki;
    public GameObject buttonName;

    // ManagerChangePlayer mcp;
    float length;
    private float speedMove = 1.5f;
    // Use this for initialization
    void Start()
    {
        BoxGame = GameObject.Find("BoxGame_" + indexGameBox);
        strzalki = GameObject.FindGameObjectsWithTag("strzalki");
        changeColor(BoxGame.GetComponent<Renderer>().material.color);
        //indexGameBox = 0;
        transform.position = new Vector3(transform.position.x, 5f * indexGameBox, transform.position.z);
        length = 5 * 5;
       
        //mcp = GameObject.Find("Manager").GetComponent<ManagerChangePlayer>();

        //TransformZ(transform.position);
    }
    private void FixedUpdate()
    {
        if (!(transform.position.y % 5f == 0))
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, PositionChange(transform.position.y), transform.position.z), Time.deltaTime * 2);
        }
        TransformZ(transform.position);
    }

    void Update()
    {
        //length = (mcp.getPlayer().listColor.Count - 1) * 2.5f;
        //Debug.Log ("move color up");
        if (Input.GetMouseButtonDown(0))
        {
            tempPos = transform.position;
            float distance = transform.position.z - Camera.main.transform.position.z;
            targetPos = new Vector3(0, Input.mousePosition.y, distance);
            targetPos = Camera.main.ScreenToWorldPoint(targetPos);
            Vector3 followXonly = new Vector3(transform.position.x, targetPos.y * speedMove, transform.position.z);
            tempY = followXonly - tempPos;
        }
        if (Input.GetMouseButton(0))
        {
            move();
        }
        
    }
    void move()
    {
        //Debug.Log ("move");
        float distance = transform.position.z - Camera.main.transform.position.z;
        targetPos = new Vector3( 0, Input.mousePosition.y, distance);
        targetPos = Camera.main.ScreenToWorldPoint(targetPos);
        Vector3 followXonly = new Vector3(transform.position.x, targetPos.y* speedMove, transform.position.z);
        transform.position = followXonly - tempY;

        if (transform.position.y >= length) 
        {
            transform.position = new Vector3(0,length, 0);
        }
        else if (transform.position.y <= 0)
        {
            transform.position = new Vector3(0, 0, 0);
        }

    }
    float PositionChange(float posY)
    {

        float top = 0;//left
        float down = 0;//right

        for (float i = 0; i <= length; i += 5f)
        {
            if (posY >= i && posY < i + 5f)
            {
                top = i;
                down = i + 5f;
            }
        }
        float wynikL = posY - top;
        float wynikR = down - posY;
        if (wynikL < wynikR)
        {
            return top;
        }
        else
        {
            return down;
        }
    }
    void TransformZ(Vector3 tempp)
    {
        //Debug.Log ("tranzform z ");
        float distance = 0;
        float posPlayer = PositionChange(tempp.y);
        float index = (posPlayer / 5f);
        //if((int)index != indexGameBox)
        {
            changeColor(BoxGame.GetComponent<Renderer>().material.color);
            //BoxGame.GetComponent<Renderer>().material.color = originalColor;
            //Debug.Log("zmiana na oryginalny kolor: "+indexGameBox+" kolor:"+originalColor);
        }
        indexGameBox = (int)index;
       
        BoxGame = GameObject.Find("BoxGame_" + index);
        /*if(BoxGame.GetComponent<Renderer>().material.color != selectGameColor)
            originalColor = BoxGame.GetComponent<Renderer>().material.color;

        BoxGame.GetComponent<Renderer>().material.color = selectGameColor;*/
        if (posPlayer == 0)
        {
            distance = tempp.y * 1.6f;//tempp.y - 1.5f;
        }
        else if (posPlayer > 0)
        {
            if (tempp.y > posPlayer)
            {
                distance =  ((tempp.y-posPlayer) * 1.6f);//(1.5f + (posPlayer - tempp.y));powrot na miejsce
            }
            else
            {
                distance =  -((tempp.y-posPlayer) * 1.6f);// tempp.y - (-posPlayer + 1.5f);

            }
        }
        //Debug.Log("distance:" + distance);
        refreshOtherPosytion();
        BoxGame.transform.position = new Vector3(BoxGame.transform.position.x, BoxGame.transform.position.y, distance );
    }


    void refreshOtherPosytion()
    {
        GameObject[] otherObject = GameObject.FindGameObjectsWithTag("BoxGame");
        foreach (GameObject go in otherObject)
        {
            if (BoxGame != go)
            {
                go.transform.position = new Vector3(-30.51f, go.transform.position.y, 4f);
            }
        }
    }
    void changeColor(Color tempColor)
    {

        foreach (GameObject st in strzalki)
        {
            for (int i = 0; i < st.transform.childCount; i++)
            {
                Transform tran = st.transform.GetChild(i);
                if ( tran.name == "Image")
                {
                    //Debug.Log("zmian koloru strzalek");
                    tran.GetComponent<Image>().color = tempColor;
                }
                else
                {
                    tran.GetComponent<ParticleSystem>().startColor = tempColor;
                }
            }
        }
       /* for (int i = 0; i < buttonName.transform.childCount; i++)
        {
            Transform tran = buttonName.transform.GetChild(i);
            buttonName.GetComponent<Text>().color = tempColor;
            if (tran.name == "Image")
            {
                Debug.Log("zmian koloru batona na dole");
                tran.GetComponent<Image>().color = tempColor;
            }
        }*/
    }
}
