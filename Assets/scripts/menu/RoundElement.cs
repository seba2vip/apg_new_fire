﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundElement : MonoBehaviour {

    int dir;
    float realRotation;
    float nextRot;
    float timeRot;
    void Start () {
        realRotation = 0;
        nextRot = 0;
        timeRot = 4f;
        dir = getZnak();
    }

	void Update () {

        //if (transform.rotation.eulerAngles.z >= realRotation  )
        //{
        //    Debug.Log("DSADASD");
        //    //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, realRotation), 0.3f);
        //    realRotation += 90;
        //    if (realRotation == 360)
        //        realRotation = 0;
        //}
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, dir*realRotation), 0.03f);
        if (Time.time > nextRot)
        {
            nextRot = Time.time + timeRot;
            dir = getZnak(); 
            realRotation += 90;
            if (realRotation == 360)
                realRotation = 0;

        }
    }
    int getZnak()
    {
        int znak = Random.Range(-1, 2);
        //Debug.Log(znak);
        return znak;
    }
}
