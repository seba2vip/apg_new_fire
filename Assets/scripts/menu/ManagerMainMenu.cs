﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Firebase;
using Firebase.Analytics;
using System;
using GoogleMobileAdsMediationTestSuite.Api;
using GoogleMobileAds.Api.Mediation.AdColony;
using GoogleMobileAds.Api.Mediation.Vungle;
using GoogleMobileAds.Api.Mediation.UnityAds;
using GoogleMobileAds.Api.Mediation.AppLovin;

public class ManagerMainMenu : MonoBehaviour
{
    public GameObject buttonName;
    public Text nameGame;
    public Text volumeText;
    public Text bonusText;
    public GameObject panelShop;
    public GameObject panelSettings;
    public GameObject panelGraphics;
    private GameObject[] buttonGraphics;
    public GameObject panelPopup;
    public Slider sliderAccept;
    private void Awake()
    {
        User.Instance.loadUser();
        User_FireUp.Instance.loadUser();
    }
    const float fpsMeasurePeriod = 0.5f;
    private int m_FpsAccumulator = 0;
    private float m_FpsNextPeriod = 0;
    private int m_CurrentFps;
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        if(m_CurrentFps < 20)
        {
            User.Instance.graphicSettings = User.GraphicSettings.Medium;
        }
        Debug.Log("Kurwa FPS" + m_CurrentFps);
    }

    private void Start()
    {
        m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
        StartCoroutine(ExecuteAfterTime(2));
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
        int hasPlayed = PlayerPrefs.GetInt("HasPlayed");

        if (hasPlayed == 0)
        {
            buttonName.SetActive(true);
            PlayerPrefs.SetInt("HasPlayed", 1);
        }
        else
        {
            buttonName.SetActive(false);
        }
        Screen.orientation = ScreenOrientation.Portrait;
       // Application.targetFrameRate = 30;
        nameGame.text = "Name Game";
        panelSettings.SetActive(false);
        panelGraphics.SetActive(false);
        panelShop.SetActive(false);
        panelPopup.SetActive(false);
        bonusText.text = "";
        if (User.Instance.volume)
            volumeText.text = "Volume On";
        else
            volumeText.text = "Volume Off";

       
    }
    private void Update()
    {
        nameGame.text = getNameGame(MoveBoxGameMenu.indexGameBox);
        if(User.Instance.bonusPoint>1)
            bonusText.text = "Bonus Score: " + User.Instance.bonusPoint.ToString();
        if(nameGame.text == "")
            buttonName.SetActive(false);
        else if(!buttonName.active)
                buttonName.SetActive(true);

        m_FpsAccumulator++;
        if (Time.realtimeSinceStartup > m_FpsNextPeriod)
        {
            m_CurrentFps = (int)(m_FpsAccumulator / fpsMeasurePeriod);
            m_FpsAccumulator = 0;
            m_FpsNextPeriod += fpsMeasurePeriod;
        
        }

        

    }
    public void buttonPlay()
    {
        //MediationTestSuite.Show();
        string gameNAme = getNameGame(MoveBoxGameMenu.indexGameBox);
        try
        {
            FirebaseAnalytics.LogEvent("game", "start", gameNAme);
        }
        catch (Exception e) { Debug.Log(e); }

        SceneManager.LoadScene("Game_" + gameNAme);
    }
    string getNameGame(int index)
    {
        string name = "";
        switch (index)
        {
            case 0:
                name = "FireUp";
                break;
            case 1:
                name = "Speed Box";
                break;
            case 2:
                name = "Splitter";
                break;
            case 3:
                name = "2048";
                break;
            case 4:
                name = "Circuit";
                break;
        }
        return name;
    }
    public void PanelSettings()
    {
        panelSettings.SetActive(true);
    }
    public void PanelShop()
    {
        panelShop.SetActive(true);
    }
    public void ButtonVolume()
    {
        if (volumeText.text == "Volume On")
        {
            User.Instance.volume = false;
            volumeText.text = "Volume Off";
        }
        else
        {
            User.Instance.volume = true;
            volumeText.text = "Volume On";
        }
        User.Instance.saveUser();
    }
    public void PanelGraphics()
    {
        
        panelGraphics.SetActive(true);
        buttonGraphics = GameObject.FindGameObjectsWithTag("bGraphics");
        SelectedColorButton();
    }
    public void ButtonExitPopup()
    {
        panelPopup.SetActive(false);
    }
    public void ButtonPopup()
    {
        panelPopup.SetActive(true);
        if (User.Instance.acceptPrivacy == true)
            sliderAccept.value = 1;
        else
            sliderAccept.value = 0;
    }
    public void ButtonAccept()
    {

        if (sliderAccept.value == 1)
        {
            AdColonyAppOptions.SetGDPRConsentString("1");
            AdColonyAppOptions.SetGDPRRequired(true);
            AppLovin.SetHasUserConsent(true);
            UnityAds.SetGDPRConsentMetaData(true);
            Vungle.UpdateConsentStatus(VungleConsent.ACCEPTED);
            User.Instance.acceptPrivacy = true;
        }
        else
        {
            AdColonyAppOptions.SetGDPRConsentString("0");
            AdColonyAppOptions.SetGDPRRequired(false);
            AppLovin.SetHasUserConsent(false);
            UnityAds.SetGDPRConsentMetaData(false);
            Vungle.UpdateConsentStatus(VungleConsent.DENIED);
            User.Instance.acceptPrivacy = false;
        }

        User.Instance.saveUser();
        panelPopup.SetActive(false);
    }
    public void ExitPanel()
    {
        panelShop.SetActive(false);
        panelSettings.SetActive(false);
    }
    public void ReturnToSettings()
    {
        panelGraphics.SetActive(false);
    }
    public void GraphicButton()
    {
        switch (EventSystem.current.currentSelectedGameObject.name)
        {
            case "ButtonHigh":
                User.Instance.graphicSettings = User.GraphicSettings.High;
                break;
            case "ButtonMedium":
                User.Instance.graphicSettings = User.GraphicSettings.Medium;
                break;
            case "ButtonLow":
                User.Instance.graphicSettings = User.GraphicSettings.Low;
                break;
        }
        User.Instance.saveUser();
        SelectedColorButton();
    }
    private void SelectedColorButton()
    {
        Debug.Log("klik button graphics");
        foreach (GameObject button in buttonGraphics)
        {
            Color colorButton = Color.white;
            if (GetNameButtonGraphics() == button.name)
                colorButton = Color.yellow;
            else
                colorButton = Color.cyan;

            for (int i = 0; i < button.transform.childCount; i++)
            {

                Transform tran = button.transform.GetChild(i);
                if (tran.name == "Image")
                    tran.GetComponent<Image>().color = colorButton;
                else if (tran.name == "Particle System")
                    tran.GetComponent<ParticleSystem>().startColor = colorButton;
                else if (tran.name == "Text")
                    tran.GetComponent<Text>().color = colorButton;
            }
        }
        
    }
    private string GetNameButtonGraphics()
    {
        switch (User.Instance.graphicSettings)
        {
            case User.GraphicSettings.High:
                return "ButtonHigh";

            case User.GraphicSettings.Medium:
                return "ButtonMedium";

            case User.GraphicSettings.Low:
                return "ButtonLow";
        }
        return "";
    }
}
