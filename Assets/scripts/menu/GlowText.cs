﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlowText : MonoBehaviour
{
    public int i = 50;
    bool up=true;

    void Update()
    {
        UpDown();
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
    void UpDown()
    {
        if (i > 220 && up)
            up = false;
        else if (i < 50)
            up = true;
        if (up)
            i++;
        else
            i--;

        transform.GetComponent<Image>().color = new Color(255, 0, 0, 150);
    }
}
