﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Effects/Gradient")]
public class UIGradient : BaseMeshEffect
{
    public Color m_color1 = Color.white;
    public Color m_color2 = Color.white;
    public Color m_color3 = Color.white;
    public Color m_color4 = Color.white;
    

   //[Range(-180f, 180f)]
     float m_angle = 0f;
    public bool m_ignoreRatio = false;

    public override void ModifyMesh(VertexHelper vh)
    {
        if(enabled)
        {
            Rect rect = graphic.rectTransform.rect;
            Vector2 dir = UIGradientUtils.RotationDir(m_angle);

            if (!m_ignoreRatio)
                dir = UIGradientUtils.CompensateAspectRatio(rect, dir);

            UIGradientUtils.Matrix2x3 localPositionMatrix = UIGradientUtils.LocalPositionMatrix(rect, dir);

            UIVertex vertex = default(UIVertex);
            for (int i = 0; i < vh.currentVertCount; i++) {
                vh.PopulateUIVertex (ref vertex, i);
                Vector2 localPosition = localPositionMatrix * vertex.position;
                //vertex.color *= Color.Lerp(m_color2, m_color1, localPosition.y);
                vertex.color *= UIGradientUtils.Bilerp(m_color1, m_color2, m_color3, m_color4, localPosition);
                vh.SetUIVertex (vertex, i);
            }
        }
    }
    private void Update()
    {
        if (m_angle >= 180)
            m_angle = -180;

        m_angle += 1;
        var graphic = GetComponent<Graphic>();
        graphic.SetVerticesDirty();
    }
}
