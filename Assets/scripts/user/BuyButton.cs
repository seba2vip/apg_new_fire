﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour
{
    public ItemType itemType;
    public Text priceText;
    private string defaultText;
    private ADManager aDManager;
    private void OnEnable()
    {

    }
    void Start()
    {
        //Debug.Log("kurwa ladowanie shopa////////////////////////////////////////////////////");
        defaultText = priceText.text;
        StartCoroutine(LoadPrice());
        aDManager = ADManager.instance;
    }
    public void OnClickBuy()
    {
        switch (itemType)
        {
            case ItemType.Remove_Ads:
                IAPManager.Instance.BuyRemove_Ads();
                break;
            case ItemType.Remove_Ads_Points_x2:
                IAPManager.Instance.BuyRemoveAds_pointx2();
                break;
            case ItemType.Points_x3:
                IAPManager.Instance.BuyPoint_x3();
                break;
            case ItemType.Points_x5_sub:
                IAPManager.Instance.BuyPoint_x5_sub();
                break;
        }
        if(User.Instance.isPremium)
            aDManager.destroyBanner();
    }
    private IEnumerator LoadPrice()
    {
        while (!IAPManager.Instance.IsInitialized())
            yield return null;

        string loadedPrice = "";
        switch (itemType)
        {
            case ItemType.Remove_Ads:
                loadedPrice = IAPManager.Instance.GetProducePriceFromStore(IAPManager.Instance._Remove_Ads);
                break;
            case ItemType.Remove_Ads_Points_x2:
                loadedPrice = IAPManager.Instance.GetProducePriceFromStore(IAPManager.Instance._Remove_Ads_pointx2);
                break;
            case ItemType.Points_x3:
                loadedPrice = IAPManager.Instance.GetProducePriceFromStore(IAPManager.Instance._Points_x3);
                break;
            case ItemType.Points_x5_sub:
                loadedPrice = IAPManager.Instance.GetProducePriceFromStore(IAPManager.Instance._Points_x5_sub);
                break;
        }
        priceText.text = defaultText + "\n" + loadedPrice;
    }

    public enum ItemType { Remove_Ads, Remove_Ads_Points_x2, Points_x3, Points_x5_sub }
}
