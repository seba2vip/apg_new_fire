﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    private static IAPManager mInstance = null;
    public static IAPManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = new IAPManager();
            }
            return mInstance;
        }
    }
    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    public string _Remove_Ads = "remove_ads";
    public string _Remove_Ads_pointx2 = "points_x2";
    public string _Points_x3 = "points_x3";
    public string _Points_x5_sub = "sub_remove_ads";

    public string Points_x5_sub_ios = "sub_remove_ads";
    private void Awake()
    {
        InitializePurchasing();
    }
    void Start()
    {

        mInstance = this;
  

        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
       
        
        if (m_StoreController != null && m_StoreExtensionProvider != null) 
        {
            return;
        }
       
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(_Remove_Ads, ProductType.NonConsumable);
        builder.AddProduct(_Remove_Ads_pointx2, ProductType.NonConsumable);
        builder.AddProduct(_Points_x3, ProductType.NonConsumable);
        builder.AddProduct(_Points_x5_sub, ProductType.Subscription);
        // builder.AddProduct(Points_x5_sub, ProductType.Subscription, new IDs(){{ Points_x5_sub_ios, AppleAppStore.Name },{ Points_x5_sub, GooglePlay.Name },
        //     });
       
        builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArN2uWHNFhDL0otLDRbQjNqa5azkj3TKVJL4yqDh5aQiAeGiUpfYgIFioAQl6o5512icFKfVXsVEQ4PMIOOqGizml042wdM/Bd/Ok4An/P3b1783idHaHwuwSzUJwpkpbFuMQyarHY3PoMHhWUurnYhjVyDdwZLsnRiRdfkyWfah1J3Er5JVL0Lwcmyg8yndQxoyQNL6S8NEUC0VTA9tUUfBWRP8UrHkPYyToLbqjD69Bxl8O0ZG+zz21ozV8SEImrwOEMP0ELv9kMZ6lWJB0ztPSkpOdY29E54a3TyksYtbO0/Facs9wo3orwd4li8NEq13MYB4ynW760hYGcTCAIQIDAQAB");
       
        UnityPurchasing.Initialize(this, builder);
       
    }
    public bool IsInitialized()
    {
        bool isInit = m_StoreController != null && m_StoreExtensionProvider != null;
        if (!isInit) {
            InitializePurchasing();
        }
        return isInit;
    }
    public void BuyRemove_Ads()
    {
        BuyProductID(_Remove_Ads);
    }
    public void BuyRemoveAds_pointx2()
    {
        BuyProductID(_Remove_Ads_pointx2);
    }
    public void BuyPoint_x3()
    {
        BuyProductID(_Points_x3);
    }
    public void BuyPoint_x5_sub()
    {
        BuyProductID(_Points_x5_sub);
    }
    public string GetProducePriceFromStore(string id)
    {
        Debug.Log("id:" + id);
        if (m_StoreController != null && m_StoreController.products != null)
            return m_StoreController.products.WithID(id).metadata.localizedPriceString;
        else
            return "";
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }
    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, _Remove_Ads, StringComparison.Ordinal))
        {
            User.Instance.isPremium = true;
            User.Instance.saveUser();
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _Remove_Ads_pointx2, StringComparison.Ordinal))
        {
            User.Instance.isPremium = true;
            User.Instance.AddBonusPoint(2);
            User.Instance.saveUser();
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _Points_x3, StringComparison.Ordinal))
        {
            User.Instance.isPremium = true;
            User.Instance.AddBonusPoint(3);
            User.Instance.saveUser();
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, _Points_x5_sub, StringComparison.Ordinal))
        {
            User.Instance.isPremium = true;
            User.Instance.AddBonusPoint(5);
            User.Instance.saveUser();
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }
        return PurchaseProcessingResult.Complete;
    }
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}
