﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class User
{
    public long hightScoreFireUp = 0;
    public int hightScoreSplitter = 0;
    public int hightScoreSpeedBox = 0;
    public int hightScore2048_x4 = 0;
    public int hightScore2048_x5 = 0;
    public int levelCurcuit = 0;
    public int levelUnlock = 0;
    public bool isPremium = false;
    private static User mInstance = null;
    public GraphicSettings graphicSettings = GraphicSettings.High;
    public Mode2048 mode2048 = Mode2048.x4;
    public bool volume = true;
    //public Dictionary<string, Payments.Product2> mPaymentsItems = new Dictionary<string, Payments.Product2>();
    public int bonusPoint = 1;
    public int countGameSplitter = 0;
    public int countGameSpeedBox = 0;
    public bool acceptPrivacy = true;

    public static User Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = new User();
            }
            return mInstance;
        }
    }
    private static string PATH_TO_SAVE = Application.persistentDataPath + "/usrData2";

    public void loadUser()
    {
        //Debug.Log ("user load");
        User u;
        if (File.Exists(PATH_TO_SAVE))
        {
            try
            {
                BinaryFormatter bf2 = new BinaryFormatter();
                FileStream file2 = File.Open(PATH_TO_SAVE, FileMode.Open);
                u = (User)bf2.Deserialize(file2);
                if (u != null)
                {
                    this.hightScore2048_x4 = u.hightScore2048_x4;
                    this.hightScore2048_x5 = u.hightScore2048_x5;
                    this.hightScoreFireUp = u.hightScoreFireUp;
                    this.hightScoreSpeedBox = u.hightScoreSpeedBox;
                    this.hightScoreSplitter = u.hightScoreSplitter;
                    this.isPremium = u.isPremium;
                    this.bonusPoint = u.bonusPoint;
                    this.volume = u.volume;
                    this.mode2048 = u.mode2048;
                    this.levelCurcuit = u.levelCurcuit;
                    this.levelUnlock = u.levelUnlock;
                    this.graphicSettings = u.graphicSettings;
                    this.mode2048 = u.mode2048;
                    this.countGameSpeedBox = u.countGameSpeedBox;
                    this.countGameSplitter = u.countGameSplitter;
                    this.acceptPrivacy = u.acceptPrivacy;
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }

    }
    public void saveUser()
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(PATH_TO_SAVE);
            bf.Serialize(file, this);
            file.Close();
        }
        catch (System.Exception e)
        {
            //saveUser ();
            Debug.Log("saving error " + e.Message);
        }
    }
    public enum GraphicSettings
    {
        High, Medium, Low
    }
    public enum Mode2048
    {
        x4, x5
    }
    public void AddBonusPoint(int amount)
    {
        if (bonusPoint != 1)
            bonusPoint += amount;
        else
            bonusPoint = amount;
        
    }
}
