﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Purchasing;


public class Payments : MonoBehaviour, IStoreListener
{
    [Serializable]
    public class Product2
    {
        public string price;
        public string name;
        public string id;
        public bool isAlreadyBought;

    }

    private static Payments mInstance = null;

    public static Payments Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = new Payments();
            }
            return mInstance;
        }
    }

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    void Start()
    {
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }
    #if UNITY_ANDROID
    public static List<string> items = new List<string>() {  "glowbox_remove_ads_points_x2", "points_x2", "points_x3", "points_x5"  };
#elif UNITY_IPHONE
    public static List<string> items = new List<string>() { "glowbox_remove_ads_points_x2", "points_x2_ios", "points_x3_ios", "points_x5_ios"};
#endif
    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        #if UNITY_ANDROID
        items = new List<string>() { "glowbox_remove_ads_points_x2", "points_x2", "points_x3", "points_x5" };
#elif UNITY_IPHONE
        items = new List<string>() { "glowbox_remove_ads_points_x2", "points_x2", "points_x3", "points_x5" };
#endif
#if UNITY_ANDROID
        builder.AddProduct("glowBox_remove_ads+points_x2", ProductType.NonConsumable);
        builder.AddProduct("points_x2", ProductType.Consumable);
        builder.AddProduct("points_x3", ProductType.NonConsumable);
        builder.AddProduct("points_x5", ProductType.NonConsumable);
        //builder.AddProduct("android.test.purchased", ProductType.NonConsumable);
        builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAswdCLUwLEP4Jjv91NOJ/mubGhOlTmiosey+UhDWBMpLf+Z5Pd/0yxNqAnfXSRolMI/6E3hXFKXwS3jrayMhjkJpnBGvBa4PqxsgZoqyRrYYL4M78kNrR7HhRC58Pv9jlJuu9dgrM0FAayFj5vDUh0TSbBwl+qHK0bjCaOrAgrfPuEmhyB5SZc7XvnFOnlAQTPdw2PT+MuWmWDK09vdvWXxTbGTgehXU/BEQdi0A4tUxFWlYPu/5Dx93Je+7KpqAcUGOvsEE0BrI47YouVp4sFizChmjdh4MJgBIdWUxri9a7MPxvyp7/aayLNYJ1cyvY5E9sNBmMBjghT5mwx9uNcwIDAQAB");
        #elif UNITY_IPHONE
        builder.AddProduct("glowBox_remove_ads+points_x2", ProductType.NonConsumable);
        builder.AddProduct("points_x2", ProductType.Consumable);
        builder.AddProduct("points_x3", ProductType.NonConsumable);
        builder.AddProduct("points_x5", ProductType.NonConsumable);
        #endif

        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void buyItem(string id)
    {
        if (id.Equals("glowBox_remove_ads+points_x2"))
        {
            BuyProductID(id);
        }
        else if (id.Equals("points_x2") ||id.Equals("points_x2_ios"))
        {
            BuyProductID(id);
        }
        else if (id.Equals("points_x3") || id.Equals("points_x3_ios"))
        {
            BuyProductID(id);
        }
        else if (id.Equals("points_x5") || id.Equals("points_x5_ios"))
        {
            BuyProductID(id);
        }
        

    }


    public void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        //User.Instance.mPaymentsItems = new Dictionary<string, Product2>();
        //foreach (string i in items)
        //{
        //    Product2 p = new Product2();
        //    Product pp = m_StoreController.products.WithID(i);
        //    //pp.availableToPurchase
        //    //Debug.Log("#" + i + " availableToPurchase " + pp.availableToPurchase + " transactionID " + pp.transactionID + " localizedTitle " + pp.metadata.localizedTitle + " localizedDescription " + pp.metadata.localizedDescription + " hasReceipt " + pp.hasReceipt);
        //    Debug.Log("# receipt " + pp.receipt);
        //    p.name = m_StoreController.products.WithID(i).metadata.localizedDescription;
        //    p.price = m_StoreController.products.WithID(i).metadata.localizedPriceString;
        //    p.id = i;
        //    //??????
        //    if (!p.id.Equals("one_m") && !p.id.Equals("ten_m"))
        //    {
        //        p.isAlreadyBought = pp.hasReceipt;
        //    }
        //    else
        //    {
        //        p.isAlreadyBought = false;
        //    }


        //    Debug.Log("OnInitialized: PASS");
        //    if (User.Instance.isPremium == false)
        //    {
        //        if (!p.name.Equals("one_m") && !p.name.Equals("ten_m"))
        //        {
        //            if (p.isAlreadyBought)
        //            {
        //                User.Instance.isPremium = true;
        //            }
        //        }
        //    }
        //   // User.Instance.mPaymentsItems[i] = p;
        //    //??????
        //}

    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        // A consumable product has been purchased by this user.
        if (String.Equals(args.purchasedProduct.definition.id, "glowBox_remove_ads+points_x2", StringComparison.Ordinal))
        {
            User.Instance.isPremium = true;
            //User.Instance.mPaymentsItems["glowBox_remove_ads+points_x2"].isAlreadyBought = true;

            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

        }
        else if (String.Equals(args.purchasedProduct.definition.id, "points_x2", StringComparison.Ordinal))
        {
            //User.Instance.mPaymentsItems["points_x2"].isAlreadyBought = true;
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, "points_x3", StringComparison.Ordinal))
        {
            //User.Instance.mPaymentsItems["points_x3"].isAlreadyBought = true;
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, "points_x5", StringComparison.Ordinal))
        {
            //User.Instance.mPaymentsItems["points_x5"].isAlreadyBought = true;
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
       /* else if (String.Equals(args.purchasedProduct.definition.id, "points_x2_ios", StringComparison.Ordinal))
        {
            User.Instance.mPaymentsItems["points_x2_ios"].isAlreadyBought = true;
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
        }
        else if (String.Equals(args.purchasedProduct.definition.id, "points_x3_ios", StringComparison.Ordinal))
        {
            User.Instance.mPaymentsItems["points_x3_ios"].isAlreadyBought = true;
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
        }
        else if (String.Equals(args.purchasedProduct.definition.id, "points_x5_ios", StringComparison.Ordinal))
        {
            User.Instance.mPaymentsItems["points_x5_ios"].isAlreadyBought = true;
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
        }
        // Or ... a subscription product has been purchased by this user.
        /*else if (String.Equals(args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The subscription item has been successfully purchased, grant this to the player.
        }
        // Or ... an unknown product has been purchased by this user. Fill in additional products here....
        */
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}
