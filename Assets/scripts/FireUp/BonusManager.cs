﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BonusManager : MonoBehaviour
{

    public static BonusManager INSTANCE;
    private static bool bonusVector;
    public static bool getbonusVector()
    {
        return bonusVector;
    }
    private static bool bonusDamageBall;
    public static bool getbonusDamageBall()
    {
        return bonusDamageBall;
    }
    private static bool bonusSlow;
    public static bool getbonusSlow() {
        return bonusSlow;
    }
    private static bool bonusSpeedBall;
    public static bool getbonusSpeedBall()
    {
        return bonusSpeedBall;
    }
    private static bool bonusTextShow;
	private static bool bonusDestroyAllBox;
    public static bool getbonusDestroyAllBox() {
        return bonusDestroyAllBox;
    }

    float timeActivBonusVector;
    float timeActivBonusSpeedBox;
    float timeActivBonusSlow;
    float timeActivBonusSpeedBall;

    float timeActivBonusVectorOrigin;
    float timeActivBonusSpeedBoxOrigin;
    float timeActivBonusSlowOrigin;
    float timeActivBonusSpeedBallOrigin;
    //public static float speedOn;
    public static float slowOn;
    public GameObject bonusText;
    private TextMeshPro textMesh;

    //float timeTextBonus;
    private void Awake()
    {
        INSTANCE = this;
    }
    void Start()
    {
        //Debug.Log("bonus manager star");
        timeActivBonusVectorOrigin = 5f;
        timeActivBonusSpeedBoxOrigin = 5f;
        timeActivBonusSlowOrigin = 5f;
        timeActivBonusSpeedBallOrigin = 5f;

        timeActivBonusVector = timeActivBonusVectorOrigin;
        timeActivBonusSpeedBox = timeActivBonusSpeedBoxOrigin;
        timeActivBonusSlow = timeActivBonusSlowOrigin;
        timeActivBonusSpeedBall = timeActivBonusSpeedBallOrigin;

        bonusVector = false;
        bonusDamageBall = false;
        bonusSlow = false;
        bonusSpeedBall = false;
        bonusDestroyAllBox = false;

        slowOn = GameManager_FireUp.speedBox - 1;
        //speedOn = GameManager_FireUp.speedBox + 1.5f;
        bonusText.SetActive(false);
        //timeTextBonus = 1;
        //bonusText.GetComponent<Text>().text = "";
        textMesh = bonusText.GetComponent<TextMeshPro>();
        textMesh.SetText("     ");

        //Debug.Log("bonus manager star end");
    }

    public void setbonusDamageBall() {
        bonusTextShow = true;
        textMesh.SetText("DAMAGE");
        BonusManager.bonusDamageBall = true;
    }

    public void setbonusVector()
    {
        bonusTextShow = true;
        textMesh.SetText("DOUBLE\n SHOT");
        BonusManager.bonusVector = true;
    }

    public void setbonusSlow()
    {
        bonusTextShow = true;
        textMesh.SetText("SLOW");
        BonusManager.bonusSlow = true;
    }

    public void setbonusSpeedBall()
    {
        bonusTextShow = true;
        textMesh.SetText("SPEED");
        BonusManager.bonusSpeedBall = true;

    }

    public void setbonusDestroyAllBox()
    {
        bonusTextShow = true;
        textMesh.SetText("BOOM");
        BonusManager.bonusDestroyAllBox = true;

    }
   
    void Update()
    {
        //Debug.Log("bonus manager");
        if (bonusVector)
        {
            timeActivBonusVector -= Time.deltaTime;
            if (timeActivBonusVector < 0)
            {

                bonusVector = false;
                timeActivBonusVector = timeActivBonusVectorOrigin;
            }
        }
        else if (bonusDamageBall)
        {
            timeActivBonusSpeedBox -= Time.deltaTime;
            if (timeActivBonusSpeedBox < 0)
            {

                bonusDamageBall = false;
                timeActivBonusSpeedBox = timeActivBonusSpeedBoxOrigin;
            }
        }
        else if (bonusSlow)
        {
            // bonusText.GetComponent<Text>().text = "SLOW";
           
            timeActivBonusSlow -= Time.deltaTime;
            if (timeActivBonusSlow < 0)
            {

                bonusSlow = false;
                timeActivBonusSlow = timeActivBonusSlowOrigin;
            }
        }
        else if (bonusSpeedBall)
        {
            //bonusText.GetComponent<Text>().text = "POWER";
            timeActivBonusSpeedBall -= Time.deltaTime;
            if (timeActivBonusSpeedBall < 0)
            {

                bonusSpeedBall = false;
                timeActivBonusSpeedBall = timeActivBonusSpeedBallOrigin;
            }
        }else if (bonusDestroyAllBox)
        {
            //bonusText.GetComponent<Text>().text = "BOOM";
           
            DestroyAllBox();
        }
        if (bonusTextShow && User.Instance.graphicSettings != User.GraphicSettings.Low) // obciąża strasznie
        {
           // Debug.Log("text bonus");
           if(!bonusText.active)
                bonusText.SetActive(true);
            bonusText.transform.localScale = Vector3.MoveTowards(bonusText.transform.localScale, new Vector3(0, 0, 0), Time.deltaTime);//new Vector3(timeTextBonus, timeTextBonus, 1);
            //timeTextBonus -= Time.deltaTime;

            if(bonusText.transform.localScale == new Vector3(0, 0, 0))// (timeTextBonus < 0)
            {
                bonusTextShow = false;
               // timeTextBonus = 1;
                bonusText.transform.localScale = new Vector3(1, 1, 1);
                bonusText.SetActive(false);
            }
           // Debug.Log("text bonus end");
        }
        if (GameManager_FireUp.isGameOver)
        {
            bonusVector = false;
            bonusDamageBall = false;
            bonusSlow = false;
            bonusSpeedBall = false;
            bonusDestroyAllBox = false;
        }
        //Debug.Log("bonus manager end");
    }
    private void DestroyAllBox()
    {
        GameObject boxToKill = GameObject.Find("DestroyBox");
        //Vector3 orginalPos = new Vector3(1.5f, 1.68f, -8);//boxToKill.transform.position;
        boxToKill.transform.position = Vector3.MoveTowards(boxToKill.transform.position, new Vector3(boxToKill.transform.position.x, boxToKill.transform.position.y, 21), 2f);
        if (boxToKill.transform.position.z >= 20)
        {
            //Debug.Log("usuwanie wszystkich boxow");
            boxToKill.transform.position = new Vector3(1.5f, 1.68f, -8);// new Vector3(boxToKill.transform.position.x, boxToKill.transform.position.y, -5);
            bonusDestroyAllBox = false;
        }
    }

}

