﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BallScript : MonoBehaviour
{
    //private Rigidbody rb;

    Vector3 temp;
    string sceneName;
    float distanceToDestroy;
    static string Shop_FireUp = "Shop_FireUp";
    void Start()
    {

        distanceToDestroy = 2;
        Scene currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        //Debug.Log (sceneName);
        temp = transform.position;
        //rb = GetComponent<Rigidbody>();
        //temp = rb.velocity;
    }

    void Update()
    {
        //Debug.Log("ballScript");
        if (transform.position.z > 15 || GameManager_FireUp.isGameOver && !sceneName.Equals(Shop_FireUp))
        {
            Destroy(transform.gameObject);
            //transform.gameObject = null;
        }
        else if (sceneName.Equals(Shop_FireUp))
        {
            if (temp.x < transform.position.x - distanceToDestroy || temp.x > transform.position.x + distanceToDestroy || temp.z < transform.position.z - distanceToDestroy || temp.z > transform.position.z + distanceToDestroy)
            {
                //Debug.Log("kill ball");
                Destroy(transform.gameObject);
            }

        }
      
        //Debug.Log("ballScript end");
    }

    private void FixedUpdate()
    {
        RaycastHit hit;

        Ray loadnigRay = new Ray(transform.position, Vector3.forward);
        Ray loadnigRayL = new Ray(transform.position, Vector3.left);
        Ray loadnigRayR = new Ray(transform.position, Vector3.right);
        //Ray loadnigRayB = new Ray(transform.position, Vector3.back);
        if (Physics.Raycast(loadnigRay, out hit, 0.4f) || Physics.Raycast(loadnigRayL, out hit, 0.1f) || Physics.Raycast(loadnigRayR, out hit, 0.1f))
        {
            if (hit.collider.tag.Equals(BoxScript.sBox) || hit.transform.tag.Equals(BoxScript.sMoving_Box) || hit.transform.tag.Equals(BoxScript.sDownUp_Box))
            {

                BoxScript bs = hit.transform.GetComponent<BoxScript>();
                Destroy(transform.gameObject);
                if (bs.pointNumber >= 0)
                {
                    if (bs.pointNumber < User_FireUp.Instance.powerLevel)
                        GameManager_FireUp.scoreUp += hit.transform.GetComponent<BoxScript>().pointNumber * User.Instance.bonusPoint;
                    else
                        GameManager_FireUp.scoreUp += User_FireUp.Instance.powerLevel * User.Instance.bonusPoint;
                    //Debug.Log("bummmmmm");
                    //hit.transform.GetComponent<AudioSource>().Play();

                    if (BonusManager.getbonusDamageBall())
                    {
                            bs.pointNumber -= User_FireUp.Instance.powerLevel * 4;
                        //bs.number.text = bs.pointNumber.ToString();
                        bs.color();
                    }
                    else
                    {
                        bs.pointNumber -= User_FireUp.Instance.powerLevel; 
                        //bs.number.text = bs.pointNumber.ToString();
                        bs.color();
                       // hit.transform.GetComponent<BoxScript>().pointNumber -= User_FireUp.Instance.powerLevel;
                    }

                    //Debug.Log("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbonus point: " + User.Instance.bonusPoint);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.tag.Equals(BoxScript.sBox) || collision.transform.tag.Equals(BoxScript.sMoving_Box) || collision.transform.tag.Equals(BoxScript.sDownUp_Box)) //WszystkiePudelka
        {
            //collision.transform.GetComponent<AudioSource>().Play();
            //.Log("fsdfsdfds");
            Destroy(transform.gameObject);
            BoxScript bs = collision.transform.GetComponent<BoxScript>();
            if (bs.pointNumber >= 0)
            {
                if (bs.pointNumber < User_FireUp.Instance.powerLevel)
                    GameManager_FireUp.scoreUp += bs.pointNumber * User.Instance.bonusPoint;
                else
                    GameManager_FireUp.scoreUp += User_FireUp.Instance.powerLevel * User.Instance.bonusPoint;
            }
            bs.pointNumber -= User_FireUp.Instance.powerLevel;

            //Debug.Log("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbonus point: " + User.Instance.bonusPoint);
        }
    }
 

}
