﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScript : MonoBehaviour
{
    Vector3 followXonly;
    int whichBonus;
    float speedBonus;
    private Rigidbody rb;
    Vector3 tempRb;
    // Use this for initialization
    void Start()
    {
        whichBonus = Random.Range(0, 5);
        speedBonus = GameManager_FireUp.speedBox;
        rb = GetComponent<Rigidbody>();
        tempRb = -rb.velocity;
    }

    // Update is called once per frame

    void Update()
    {

        if (!GameManager_FireUp.isGameOver)
        {
            if (BonusManager.getbonusSlow())
            {
                speedBonus = BonusManager.slowOn;
            }
            else 
            {
                speedBonus = GameManager_FireUp.speedBox;
            }

            rb.velocity = -speedBonus * (tempRb.normalized);
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (transform.tag.Equals("BonusP") && other.transform.tag.Equals("Player"))
        {
            switch (whichBonus)
            {
                case 0:
                    BonusManager.INSTANCE.setbonusVector();
                    break;
                case 1:
                    BonusManager.INSTANCE.setbonusDamageBall();
                    break;
                case 2:
                    BonusManager.INSTANCE.setbonusSlow();
                    break;
                case 3:
                    BonusManager.INSTANCE.setbonusSpeedBall();
                    break;
                case 4:
                    BonusManager.INSTANCE.setbonusDestroyAllBox();

                    break;
            }
           
        }


        Destroy(transform.gameObject);
    }
}
