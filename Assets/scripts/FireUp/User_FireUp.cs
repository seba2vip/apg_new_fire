﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class User_FireUp  {

    private static User_FireUp mInstance = null;
    public long overallScore = 0;
	public long powerLevel = 1;
	public long speedLevel = 1;

    public string namePlayer = "Player_00";                  // na start
    public int countShotPlayer = 1;//new Color(0, 213, 255, 255); //  --//--

    public string nameBall = "Ball_0";                      // na start
    public int colorBall = 4;//new Color(0, 255, 0, 255);     //  --//--

    

	public List<CharacterToBuy> listPlayer;  //  postacie
	public List<ItemToBuy> listBall;    //  ksztalty kulek

    private List<ShootToBuy> Player_0; 
    //private List<ColorToBuy> Player_5;
   // private List<ColorToBuy> Player_6;
   // private List<ColorToBuy> Player_7;
    //lista kolorów do danej postaci
    // private List<ColorToBuy> Player_1;  //  --//--
    // private List<ColorToBuy> Player_2;  //  --//--
    // private List<ColorToBuy> Player_3;  //  --//--
    //private List<ColorToBuy> Player_4;  //  --//--

    private List<ColorToBuy> listBallColor;      // lista kolorow do kulek narazie do wszystkich kulek taka sama lista


    //public List<ColorToBuy> listPlayerColor;    // lista kolorów do danej postacie
    //public List<List<ColorToBuy>> list

    private static string PATH_TO_SAVE = Application.persistentDataPath + "/usrData112";
    public static User_FireUp Instance
    {
        get
        {
            if (mInstance == null)
            {
                mInstance = new User_FireUp();
                mInstance.init();
                //mInstance.loadUser();
            }
            return mInstance;
        }
    }
    public void saveUser(){
        Debug.Log("save fireup&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		try
		{
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Create(PATH_TO_SAVE);
            bf.Serialize(file, this);
            //bf.Deserialize(file, User_FireUp);
           
            file.Close();
		}
		catch (System.Exception e) {
			//saveUser ();
			Debug.Log("saving error " + e.Message);
		}
	}


	public void loadUser()
    {
        Debug.Log("load fireup&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");

        //Debug.Log ("user load");
        User_FireUp u;
        if (File.Exists(PATH_TO_SAVE))
        {
            try
            {
                BinaryFormatter bf2 = new BinaryFormatter();
            FileStream file2 = File.Open(PATH_TO_SAVE, FileMode.Open);
            u = (User_FireUp)bf2.Deserialize(file2);
                if (u != null)
                {
                    this.overallScore = u.overallScore;
                    this.powerLevel = u.powerLevel;
                    this.speedLevel = u.speedLevel;
                    this.namePlayer = u.namePlayer;
                    this.countShotPlayer = u.countShotPlayer;
                    this.nameBall = u.nameBall;
                    this.colorBall = u.colorBall;
                    this.listPlayer = u.listPlayer;
                    this.listBall = u.listBall;
                    this.Player_0 = u.Player_0;
                   // this.Player_5 = u.Player_5;
                    //this.Player_6 = u.Player_6;
                    //this.Player_7 = u.Player_7;
                    this.listBallColor = u.listBallColor;
                }
                else
                {
                    //init();
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
                //init();
            }
        }
		 else
        {
            //init();
		}

        this.overallScore = 10000000;
    }

	public long getSpeedUpCost(){
		long cost = 4;
        float mult = 1.2f;
        if (User_FireUp.Instance.speedLevel > 75)
        {
            mult = 40;
        }
        if (User_FireUp.Instance.speedLevel > 50)
        {
            mult = 20;
        }
        else if (User_FireUp.Instance.speedLevel > 30)
        {
            mult = 7;
        }
        else if (User_FireUp.Instance.speedLevel > 20)
        {
            mult = 5.0f;
        }
        else if (User_FireUp.Instance.speedLevel > 10)
        {
            mult = 2.2f;
        }

        cost *= ((long)((6 * speedLevel) * (mult + (User_FireUp.Instance.speedLevel * 0.3)) * User_FireUp.Instance.speedLevel/2) /* ???? * speedLevel*/);

		return cost/2;
	}

	public bool buySpeedUp(){
		long cost = getSpeedUpCost ();

		if (overallScore >= cost ) {
			overallScore -= cost;
			speedLevel++;
			saveUser ();
			return true;
		} else {
			return false;
		}


        
            
    }
    //static int r = 254;
    //static int g = 0;
    //static int b =  0;

    public long getPowerUpCost(){
		long cost = 4;
        float mult = 1.2f;
        if (User_FireUp.Instance.powerLevel > 25)
        {
            mult = 40;
        }
        if (User_FireUp.Instance.powerLevel > 20)
        {
            mult = 20;
        }
        else if (User_FireUp.Instance.powerLevel > 15)
        {
            mult = 7;
        }
        else if (User_FireUp.Instance.powerLevel > 10)
        {
            mult = 5.0f;
        }
        else if (User_FireUp.Instance.powerLevel > 5)
        {
            mult = 2.2f;
        }

        cost *= ((long)((6 * powerLevel) * (mult + (User_FireUp.Instance.powerLevel* 0.3)) * User_FireUp.Instance.powerLevel/2)/* ???? * speedLevel*/);

		return cost;
	}

	public bool buyPowerUp(){
		long cost = getPowerUpCost ();

		if (overallScore >= cost) {
			overallScore -= cost;
			powerLevel++;
			saveUser ();
			return true;
		} else {
			return false;
		}
	}

	public long getcurrentDifficult(){
        long diff = 1+(User_FireUp.Instance.powerLevel/5)  + (User_FireUp.Instance.powerLevel * User_FireUp.Instance.speedLevel / 4);
        return diff ;//* diff;

	}
    
    [System.Serializable]
    public class ItemToBuy{
		public string name;
		public TypeOwn type;
		public int cost;
        public List<ColorToBuy> listColor;

		public ItemToBuy(string name, TypeOwn type, int cost, List<ColorToBuy> listColor)
        {
			this.name = name;
			this.type = type;
			this.cost = cost;
            this.listColor = listColor;
		}
		public int Getcost()
		{
			return cost;
		}
        public List<ColorToBuy> GetlistColor()
        {
            return listColor;
        }
    }
    [System.Serializable]
    public class CharacterToBuy
    {
        public string name;
        public TypeOwn type;
        public int cost;
        public List<ShootToBuy> listCount;

        public CharacterToBuy(string name, TypeOwn type, int cost, List<ShootToBuy> listCount)
        {
            this.name = name;
            this.type = type;
            this.cost = cost;
            this.listCount = listCount;
        }
        public int Getcost()
        {
            return cost;
        }
        public List<ShootToBuy> GetlistCount()
        {
            return listCount;
        }
    }
    [System.Serializable]
    public class ColorToBuy{
		public int color;
		public TypeOwn type;
		public int cost;
        //public Material material;

		public ColorToBuy(int color, TypeOwn type, int cost ){
			this.color = color;
			this.type = type;
			this.cost = cost;
            //this.material = material;
		}
	}
    [System.Serializable]
    public class ShootToBuy
    {
        public int countShot;
        public TypeOwn type;
        public int cost;
        //public Material material;

        public ShootToBuy(int countShot, TypeOwn type, int cost)
        {
            this.countShot = countShot;
            this.type = type;
            this.cost = cost;
            //this.material = material;
        }
    }
    public enum TypeOwn{
		ToBuy, Bought, Chosen
	}

	public void init(){
       
        //Debug.Log ("listy");
        Player_0 = new List<ShootToBuy>();
        Player_0.Add(new ShootToBuy(1, TypeOwn.Chosen, 0));
        Player_0.Add(new ShootToBuy(2, TypeOwn.ToBuy, 10000));
        Player_0.Add(new ShootToBuy(3, TypeOwn.ToBuy, 80000));
        Player_0.Add(new ShootToBuy(4, TypeOwn.ToBuy, 250000));
        Player_0.Add(new ShootToBuy(5, TypeOwn.ToBuy, 500000));

        /*Player_5 = new List<ColorToBuy>();
        Player_5.Add(new ColorToBuy(-1, TypeOwn.Chosen, 0));

        Player_6 = new List<ColorToBuy>();
        Player_6.Add(new ColorToBuy(5, TypeOwn.Chosen, 0));

        Player_7 = new List<ColorToBuy>();
        Player_7.Add(new ColorToBuy(-1, TypeOwn.Chosen, 0));*/


        listPlayer = new List<CharacterToBuy>();
		listPlayer.Add(new CharacterToBuy("Player_00", TypeOwn.Chosen, 0, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_01", TypeOwn.ToBuy, 1000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_02", TypeOwn.ToBuy, 10000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_03", TypeOwn.ToBuy, 10000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_04", TypeOwn.ToBuy, 10000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_05", TypeOwn.ToBuy, 100000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_06", TypeOwn.ToBuy, 100000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_07", TypeOwn.ToBuy, 100000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_08", TypeOwn.ToBuy, 100000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_09", TypeOwn.ToBuy, 100000, Player_0));
        listPlayer.Add(new CharacterToBuy("Player_10", TypeOwn.ToBuy, 100000, Player_0));



        listBallColor = new List<ColorToBuy>();
        listBallColor.Add(new ColorToBuy(4, TypeOwn.Chosen, 0));
        listBallColor.Add(new ColorToBuy(1, TypeOwn.ToBuy, 1000));
        listBallColor.Add(new ColorToBuy(2, TypeOwn.ToBuy, 1000));
        listBallColor.Add(new ColorToBuy(3, TypeOwn.ToBuy, 1000));

        listBall = new List<ItemToBuy>();
		listBall.Add(new ItemToBuy("Ball_0", TypeOwn.Chosen, 0, listBallColor));
		listBall.Add(new ItemToBuy("Ball_1", TypeOwn.ToBuy, 10000, listBallColor));
		listBall.Add(new ItemToBuy("Ball_2", TypeOwn.ToBuy, 10000, listBallColor));

	}
    public Color GetColor(int index)
    {
        switch (index)
        {
            case 0:
                return Color.cyan;
                break;

            case 1:
                return Color.blue;
                break;

            case 2:
                return Color.yellow;
                break;

            case 3:
                return Color.red;
                break;
            case 4:
                return Color.green;
                break;
            case 5:
                return Color.black;
                break;
        }
        return Color.white;
    }
}

