﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBoxEffect : MonoBehaviour {
    //AudioClip audiobum;
    float czas = 3f;
    private void Start()
    {
        //audiobum = (AudioClip)Resources.Load("FireUp/bum");
        //GetComponent<AudioSource>().volume = 0.05f;
        //GetComponent<AudioSource>().playOnAwake = false;
        //GetComponent<AudioSource>().clip = audiobum;
        if(User.Instance.volume)
            GetComponent<AudioSource>().Play();
    }
    void Update()
    {
        if (czas < 0)
        {
            
            Destroy(transform.gameObject);
        }
        czas -= Time.deltaTime;
    }
}
