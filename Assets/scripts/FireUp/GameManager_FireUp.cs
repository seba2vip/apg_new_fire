﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class GameManager_FireUp : MonoBehaviour {
    public GameObject gameOver;
    public GameObject playButton;
    //public GameObject secondChandeButton;
    //public GameObject boxDestroy;
    public GameObject panelUpgrade;
    public GameObject buttonSpeedUpGO;
    public GameObject buttonPowerUpGO;
    public Text scoreText;
    public Text hightScoreText;
    public Text lastScoreText;
    public static bool isGameOver;
    public static bool canAdd;
    public static long scoreUp;
    public static float speedBox = 3.5f;
    private static User_FireUp user_FireUp;
    public static bool isFirst = false;
    //public Image BarSeconChance;
    //bool scondChance;
    //float timeToSecondChance;
    GameObject[] playerGO;
    public Text levelPower;
    public Text levelSpeed;
    public Text allScore;
    //long lastScore;
    //float tempSizeFront;
    //float originalSizeFront;
    //bool transformFronScoreUp;
    //bool transformFronScoreDown;
    ColorBlock cbpower;
    ColorBlock cbspeed;
    //public GameObject settingsPanel;
    private static int counterGameToFullAD;
    //private static long lastScore;
    private void Awake()
    {
        User_FireUp.Instance.loadUser();
    }
    void Start()
    {
        //lastScore = 0;
        counterGameToFullAD = 0;
        if (User.Instance.graphicSettings == User.GraphicSettings.High)
        {
            GameObject.Find("Main Camera").GetComponent<Bloom>().enabled = true;
            GameObject.Find("Main Camera").GetComponent<MobileBloom>().enabled = false;
        }
        else if(User.Instance.graphicSettings == User.GraphicSettings.Medium)
        {
            
            GameObject.Find("Main Camera").GetComponent<Bloom>().enabled = false;
            GameObject.Find("Main Camera").GetComponent<MobileBloom>().enabled = true;
        }
        else
        {
            GameObject.Find("Main Camera").GetComponent<Bloom>().enabled = false;
            GameObject.Find("Main Camera").GetComponent<MobileBloom>().enabled = false;
        }
        //settingsPanel.SetActive(false);
        cbpower = buttonPowerUpGO.GetComponent<Button>().colors;
        cbspeed = buttonSpeedUpGO.GetComponent<Button>().colors;
        isGameOver = true;
        Refresh();
        //timeToSecondChance = 5f;
        /*if (isFirst) {
			isGameOver = true;
			isFirst = false;
			gameOver.SetActive (true);
		} else {
			
		}*/
        counterGameToFullAD = 0;
       // transformFronScoreUp = false;
       // transformFronScoreDown = false;
        panelUpgrade.SetActive(false);
        gameOver.SetActive(true);
        //secondChandeButton.SetActive(false); // do secondchance on/off
        playButton.SetActive(true); // do secondchance on/off
        hightScoreText.enabled = false;
        lastScoreText.enabled = false;
        scoreUp = 0;
        //lastScore = User.Instance.hightScoreFireUp;
       // tempSizeFront = scoreText.GetComponent<Text>().resizeTextMaxSize;
        //originalSizeFront = scoreText.GetComponent<Text>().resizeTextMaxSize;
        scoreText.text = Conventer(User_FireUp.Instance.overallScore);

        hightScoreText.text = "Last Score \n" + Conventer(scoreUp); // user zapis ostatniej ilosci punktow i max
        lastScoreText.text = "Last Score\n" + Conventer(scoreUp);
        if (!ADManager.isPremium)
            ADManager.instance.CreateInterstitial();

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("manager");
        //if (canAdd)
        {
            if (isGameOver)
            {
                if (canAdd)
                {
                    Debug.Log("koniec");
                    canAdd = false;
                    User_FireUp.Instance.overallScore += scoreUp; // do poprawy w innym mijscu ??
                    User_FireUp.Instance.saveUser();
                    scoreText.text = Conventer(User_FireUp.Instance.overallScore);
                    if (User.Instance.hightScoreFireUp < scoreUp)
                    {
                        User.Instance.hightScoreFireUp = scoreUp;
                    }
                    hightScoreText.text = "Hight Score \n" + Conventer(User.Instance.hightScoreFireUp);
                    lastScoreText.text = "Last Score \n" + Conventer(scoreUp);
                    hightScoreText.enabled = true;
                    lastScoreText.enabled = true;
                    gameOver.SetActive(true);
                    if (!ADManager.isPremium)           //reklamy
                    {
                        Debug.Log("SDADADADasdsaaaaadddddddddd");
                        if (User_FireUp.Instance.speedLevel < 10 && User_FireUp.Instance.powerLevel < 10)
                        {
                            counterGameToFullAD++;
                            if (counterGameToFullAD >= 3)
                            {
                                try
                                {
                                    ADManager.instance.Display_InterstitialAD();
                                    counterGameToFullAD = 0;
                                   
                                }
                                catch (Exception e) { }
                                
                            }
                        }
                        else
                        {
                            try
                            {
                                ADManager.instance.Display_InterstitialAD();
                               
                            }
                            catch (Exception e) { }
                            
                        }
                    }
                    //scondChance = true;
                    /*if(timeToSecondChance > 4 )
                    {
                        boxDestroy.transform.position = new Vector3(1.5f, 0, 0);
                    }*/
                }
            }
            else
            {
                scoreText.text = Conventer(scoreUp);//scoreUp.ToString();
                canAdd = true;
            }
        }
        /*if (scondChance)
        {
            
            timeToSecondChance -= Time.deltaTime;
            BarSeconChance.fillAmount = timeToSecondChance / 5;
            if (timeToSecondChance < 0)
            {
                scondChance = false;
                secondChandeButton.SetActive(false);
                playButton.SetActive(true);
            }
        }*/
        /*if (User.Instance.graphicSettings != User.GraphicSettings.Low)
        {
            if (!isGameOver)
            {
                //Debug.Log ("score text");
                if (User.Instance.hightScoreFireUp < scoreUp && !transformFronScoreUp && !transformFronScoreDown)
                {
                    tempSizeFront = scoreText.GetComponent<Text>().resizeTextMaxSize;
                    transformFronScoreUp = true;
                    User.Instance.hightScoreFireUp = scoreUp;
                }
                if (transformFronScoreUp)
                {
                    if ((originalSizeFront * 1.4f) >= tempSizeFront)
                    {
                        tempSizeFront += originalSizeFront / 3;
                    }
                    else
                    {
                        transformFronScoreUp = false;
                        transformFronScoreDown = true;
                    }
                }
                else if (transformFronScoreDown)
                {
                    if (originalSizeFront <= tempSizeFront)
                    {
                        tempSizeFront -= 8;
                    }
                    else
                    {
                        transformFronScoreDown = false;
                    }
                }
                //Debug.Log ("score text end");
                scoreText.GetComponent<Text>().resizeTextMaxSize = (int)tempSizeFront;
            }
            else
            {
                scoreText.GetComponent<Text>().resizeTextMaxSize = (int)originalSizeFront;
            }
        }*/

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitButton();
        }
        //Debug.Log("manager end");
    }



    /*public static User_FireUp getUser()
    {
        if (user_FireUp == null)
        {
            user_FireUp = User_FireUp.Instance;
        }

        return user_FireUp;
    }*/


    public void restart()
    {
        hightScoreText.enabled = false;
        lastScoreText.enabled = false;
        //tempSizeFront = originalSizeFront;
        //lastScore = User.Instance.hightScoreFireUp;
        SpawnBox.countOfSpawnedLines = 0;
        //lastScore = scoreUp;
        scoreUp = 0;
        gameOver.SetActive(false);
        isGameOver = false;

    }
    public void SecondChanceButton()
    {

        gameOver.SetActive(false);
        //secondChandeButton.SetActive(false);
        playButton.SetActive(true);
        isGameOver = false;
        //boxDestroy.transform.position = new Vector3(1.5f, 0, -5);

    }
    public void SpeedUpButton()
    {
        //kupowanie
        User_FireUp.Instance.buySpeedUp();
        Refresh();
    }
    public void PowerUpButton()
    {
        //kupowanie
        User_FireUp.Instance.buyPowerUp();
        Refresh();
    }
    public void StarButton()
    {

    }
    public void NoAdsButton()
    {

    }
    void DeleteBoxToSecondChance()
    {


    }
    public void ChangePlayerButton()
    {
        SceneManager.LoadScene("Shop_FireUp");
    }
    public void ExitButton()
    {
        SceneManager.LoadScene("MainMenu");
    }

    /*void SpawnPlayer()
    {
        //Debug.Log("")
        playerGO = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < playerGO.Length; i++)
        {
            Destroy(playerGO[i]);
        }
        GameObject prefab = (GameObject)Instantiate(Resources.Load(ManagerFireUp.getUser().namePlayer, typeof(GameObject)) as GameObject);
        prefab.GetComponent<Renderer>().material.color = ManagerFireUp.getUser().colorPlayer;
        prefab.transform.position = new Vector3(1.05f, 0.2f, 0);
    }*/
    public void Upgrade()
    {
        
        panelUpgrade.SetActive(true);
        Refresh();
    }
    void Refresh()
    {
        //Debug.Log("refresh");

        if (User_FireUp.Instance.overallScore >= User_FireUp.Instance.getPowerUpCost())
        {

            cbpower.selectedColor = new Color(255, 255, 0, 255);
            cbpower.normalColor = new Color(255, 255, 0, 255);
            cbpower.highlightedColor = new Color(149, 149, 0, 255);

            buttonPowerUpGO.GetComponent<Button>().colors = cbpower;
           

            //levelPower.color = Color.white;
        }
        else
        {
            cbpower.selectedColor = new Color(255, 0, 0, 255);
            cbpower.normalColor = new Color(255, 0, 0, 255);
            cbpower.highlightedColor = new Color(255, 0, 0, 255);
            buttonPowerUpGO.GetComponent<Button>().colors = cbpower;
            //levelPower.color = Color.red;
        }
        if (User_FireUp.Instance.overallScore >= User_FireUp.Instance.getSpeedUpCost() )
        {
            cbspeed.selectedColor = new Color(0, 255, 255, 255);
            cbspeed.normalColor = new Color(0, 255, 255, 255);
            cbspeed.highlightedColor = new Color(0, 149, 145, 255);
            buttonSpeedUpGO.GetComponent<Button>().colors = cbspeed;
           
            //levelSpeed.color = Color.white;
        }
        else
        {
            cbspeed.selectedColor = new Color(255, 0, 0, 255);
            cbspeed.normalColor = new Color(255, 0, 0, 255);
            cbspeed.highlightedColor = new Color(255, 0, 0, 255);
            buttonSpeedUpGO.GetComponent<Button>().colors = cbspeed;
            //levelSpeed.color = Color.red;
        }
        //if(User_FireUp.Instance.speedLevel < 41)
            levelSpeed.text = "Shooting\n Speed\n Level " + User_FireUp.Instance.speedLevel.ToString() + "\n" + Conventer(User_FireUp.Instance.getSpeedUpCost());
        //else
        //    levelSpeed.text = "Shooting\n Speed\n Max\n Level ";

        levelPower.text = "Shooting\n Damage\n Level " + User_FireUp.Instance.powerLevel.ToString() + "\n" + Conventer(User_FireUp.Instance.getPowerUpCost());
        
        allScore.text = Conventer(User_FireUp.Instance.overallScore);//  ManagerFireUp.getUser().overallScore.ToString();
        hightScoreText.text = "Hight Score \n" + Conventer(User.Instance.hightScoreFireUp);
        lastScoreText.text = "Last Score \n" + Conventer(scoreUp);
       // Debug.Log("refresh end");
    }
    public void ExitUpgradeButton()
    {
        panelUpgrade.SetActive(false);
        //settingsPanel.SetActive(false);
    }
    public string Conventer(long sc)
    {
        //Debug.Log("conventer");
        string scT = "";
        if (sc >= 1000 && sc < 1000000)
        {
            scT = (sc / 1000).ToString() + "." + ((sc % 1000) / 10).ToString() + "K";
        }
        else if (sc >= 1000000)
        {
            scT = (sc / 1000000).ToString() + "MLN";
        }
        else
        {
            scT = sc.ToString();
        }
        return scT;
        //Debug.Log ("conventer end");
    }
    /*public void SettingsButton()
    {
        settingsPanel.SetActive(true);
    }
    public void SettingsGraphicButton()
    {
        switch (EventSystem.current.currentSelectedGameObject.name)
        {
            case "H":
                User.Instance.graphicSettings = User.GraphicSettings.High;
                GameObject.Find("Main Camera").GetComponent<Bloom>().enabled = true;
                break;
            case "N":
                User.Instance.graphicSettings = User.GraphicSettings.Medium;
                GameObject.Find("Main Camera").GetComponent<Bloom>().enabled = true;
                break;
            case "L":
                User.Instance.graphicSettings = User.GraphicSettings.Low;
                GameObject.Find("Main Camera").GetComponent<Bloom>().enabled = false;
                break;
        }
    }*/

}
