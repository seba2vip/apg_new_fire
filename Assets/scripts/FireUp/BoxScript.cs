﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoxScript : MonoBehaviour
{
    //private AudioClip audiobum;
    public static string sBox = "Box";
    public static string sMoving_Box = "Moving_Box";
    public static string sDownUp_Box = "DownUp_Box";
    public static string sEmptyBox = "EmptyBox";
    
    private Rigidbody rb;
    public GameObject effectBox;
    GameObject numberGO;
    public GameObject number;
    Vector3 followXonly;
    int colorIndex;
    int colorIndexSave;
    float speedBox;
    //float speedBoxConst = GameManager.speedBox;
    public long pointNumber;
    bool first;
    bool downTransform;

    public static long randomPoint = 1;
    public static System.Random rand = new System.Random();// randomPoint = 1;

    int tempDeff = 5;       //Do zakresu punktow na kostkach oryginal - tempDeff * oryginal
    float moveXY = -1.5f;   //Do transformacji golra/dol/lewo/prawo
    float tempY;        //Do transformacji opadanie klocka
    public Material e1;
    public Material e2;
    public Material e3;
    public Material e4;
    public Material c1;
    public Material c2;
    public Material c3;
    public Material c4;
    private Renderer rendererMain;
    private Renderer effectRenderer;

    void Start()
    {
        if (transform.tag == BoxScript.sBox)
        {
            //audiobum = (AudioClip)Resources.Load("FireUp/bum");
            //GetComponent<AudioSource>().playOnAwake = false;
            //GetComponent<AudioSource>().clip = audiobum;
        }
        //Debug.Log("licznik: "+GameManager.getUser().getcurrentDifficult());
        rb = GetComponent<Rigidbody>();
        downTransform = false;
        colorIndex = 1;
        colorIndexSave = 1;
        //effectRenderer = effectBox.GetComponent<Renderer>();
        rendererMain = gameObject.GetComponent<Renderer>();
        //Debug.Log("SpawnBox.countOfSpawnedLines " + SpawnBox.countOfSpawnedLines);

        //if (SpawnBox.countOfSpawnedLines < 4 && User_FireUp.Instance.powerLevel > 1 && User_FireUp.Instance.speedLevel > 1)//początkowe figury
        //{
        //    long nr = User_FireUp.Instance.getcurrentDifficult();
        //    switch (SpawnBox.countOfSpawnedLines)
        //    {
        //        case 1:
        //            pointNumber = nr;
        //            break;
        //        case 2:
        //            pointNumber = 2 * nr;
        //            if (transform.position.y == 1.5f)
        //            {
        //                pointNumber = nr;
        //            }
        //            break;
        //        case 3:
        //            pointNumber = 3 * nr;
        //            if (transform.position.y == 1.5f)
        //            {
        //                pointNumber = 2 * nr;
        //            }
        //            else if (transform.position.y == 2.5f)
        //            {
        //                pointNumber = nr;
        //            }
        //            break;
        //    }

        //}
        //else
        //{
        //Debug.Log("range "+ (User_FireUp.Instance.getcurrentDifficult()+" -- "+ ((SpawnBox.countOfSpawnedLines / 3) * User_FireUp.Instance.getcurrentDifficult())));

        long cd = User_FireUp.Instance.getcurrentDifficult();
        float range = ((SpawnBox.countOfSpawnedLines / 5) * cd);
        if (range <= 0)
        {
            range = cd;
        }
        range += range * 1.2f * User_FireUp.Instance.countShotPlayer;
        //Random.InitState(System.Guid.NewGuid().GetHashCode());

        pointNumber = (long)(rand.Next(( 1 ), (int)range));

        //if (range > 1)
        //{
        //    while (randomPoint == pointNumber)
        //    {
        //        Random.InitState(System.DateTime.Now.Millisecond);
        //        Debug.Log("rrange:  " + (User_FireUp.Instance.getcurrentDifficult()) + " % " + range);
        //        pointNumber = (long)(rand.Next((int)(User_FireUp.Instance.getcurrentDifficult()), (int)range));
        //    }
        //    }

        //    if (pointNumber <= 0) {
        //    pointNumber = 1;
        //}
        //randomPoint = pointNumber;
        // Debug.Log("pointNumber  " + pointNumber);
      //  Debug.Log("rrange:  " + (1) + " % " + range + " kurwa " + pointNumber);
        pointNumber += (int)((SpawnBox.countOfSpawnedLines* (User_FireUp.Instance.getcurrentDifficult())) / 100);
        
        //         Debug.Log("pointNumber ## " + pointNumber);
        //  Debug.Log(" kurwa true" + pointNumber);


        //   }
        if (transform.tag.Equals(BoxScript.sBox) || transform.tag.Equals(BoxScript.sMoving_Box) || transform.tag.Equals(BoxScript.sDownUp_Box))
        {
            //Debug.Log("getcurrentdiff:" + GameManager.getUser().getcurrentDifficult());
            //numberGO = transform.Find("Canvas/Text").gameObject; //pisanie na kostce
            //if (numberGO != null)
            //{
            //number = numberGO.GetComponent<Text>();
            //Debug.Log("nazwa klocka" + number + " nr:" + pointNumber);
            number.GetComponent<TextMeshProUGUI>().SetText("sss");
                //number.text = pointNumber.ToString();
            //}

            
            first = true;
            color();
        }
        speedBox = GameManager_FireUp.speedBox;
    }


    void Update()
    {
        //bool isBox = transform.tag.Equals(BoxScript.sBox) || transform.tag.Equals(BoxScript.sMoving_Box) || transform.tag.Equals(BoxScript.sDownUp_Box);

        if (!GameManager_FireUp.isGameOver)
        {
            if (transform.tag.Equals(BoxScript.sMoving_Box))
            {
                //Debug.Log("moving box to:"+moveXY);
                if (transform.position.x >= SpawnBox.sizeLine - 1.5f)//2.5f) 
                {
                   
                    moveXY = -1.5f;
                  //  Debug.Log("moving on left:" + moveXY);
                }
                else if (transform.position.x <= -0.5f)
                {
                    
                    moveXY = SpawnBox.sizeLine + 0.5f;//4.5f;
                //    Debug.Log("moving to right:"+moveXY);
                }
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(moveXY, transform.position.y, transform.position.z), 0.015f);
            }
            else if (transform.tag.Equals(BoxScript.sDownUp_Box))
            {
                //Debug.Log(BoxScript.sDownUp_Box);
                if (transform.position.y >= 4.5f)
                {
                    //Debug.Log("moving on down");
                    moveXY = -2.5f;
                }
                else if (transform.position.y <= 0.5f)
                {
                    //Debug.Log("moving on right");
                    moveXY = 6.5f;
                }
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, moveXY, transform.position.z), 0.015f);
            }


            rb.velocity = -speedBox * (new Vector3(0, 0, 1).normalized);//-speedBox * (tempRb.normalized);

            if (BonusManager.getbonusSlow())
            {
                speedBox = BonusManager.slowOn;
            }
            else
            {
                speedBox = GameManager_FireUp.speedBox;
            }
        }
        else
        {
            rb.velocity = new Vector3(0, 0, 0);
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, transform.position.z), 0);
        }
        if (pointNumber <= 0 ||BonusManager.getbonusDestroyAllBox())
        {
            if(!transform.tag.Equals(sEmptyBox))
                finishBox();
        }
        
    }

    private void FixedUpdate()
    {
        bool isBox = transform.tag.Equals(BoxScript.sBox) || transform.tag.Equals(BoxScript.sMoving_Box) || transform.tag.Equals(BoxScript.sDownUp_Box);
        if (isBox)
        {
            float temp = (int)(transform.position.x * 10);
            temp /= 10;
            if (transform.position.y > 1 && !downTransform && temp % 0.5f == 0 && temp %1 != 0 && !transform.tag.Equals(BoxScript.sDownUp_Box))
            {
                //Debug.Log("downtransform spr");
                RaycastHit hit;
                Ray loadnigRay = new Ray(transform.position, Vector3.down);
                if (!Physics.Raycast(loadnigRay, out hit, 1))
                {
                    tempY = transform.position.y - 1;
                    transform.gameObject.tag = BoxScript.sBox;
                    downTransform = true;
                }
                //Debug.Log("downtransform spr end");
            }
            if (downTransform)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(temp, tempY, transform.position.z), 0.5f);//Time.deltaTime * 10
                if (transform.position.y <= tempY + 0.01f)
                {
                    transform.position = new Vector3(transform.position.x, tempY, transform.position.z);
                    downTransform = false;
                }
            }
        }
    }

   
    void finishBox()
    {

        if (BonusManager.getbonusDestroyAllBox())
            GameManager_FireUp.scoreUp += pointNumber * User.Instance.bonusPoint;

        /*if (e1 == null)
        {
            e1  = Resources.Load("FireUp/ColorBoxEffect1", typeof(Material)) as Material;
            e2  = Resources.Load("FireUp/ColorBoxEffect2", typeof(Material)) as Material;
            e3  = Resources.Load("FireUp/ColorBoxEffect3", typeof(Material)) as Material;
            e4  = Resources.Load("FireUp/ColorBoxEffect4", typeof(Material)) as Material;

        }*/
        
        GameObject effect = effectBox;//Resources.Load("FireUp/BoxEffect", typeof(GameObject)) as GameObject;
        if (effectBox != null)
        {
            if (colorIndexSave == 1)
            {
                effect.GetComponent<Renderer>().material = e1;
            }
            else if (colorIndexSave == 2)
            {
                effect.GetComponent<Renderer>().material = e2;
            }
            else if (colorIndexSave == 3)
            {
                effect.GetComponent<Renderer>().material = e3;
            }
            else
            {
                effect.GetComponent<Renderer>().material = e4;
            }
            if (User.Instance.graphicSettings != User.GraphicSettings.Low)
            {
                Instantiate(effect, transform.position, Quaternion.identity);
            }
        }
        Destroy(transform.gameObject);
    }

   
    public void color(){
        number.GetComponent<TextMeshProUGUI>().SetText(pointNumber.ToString());
        /*if (c1 == null) {
             c1 = Resources.Load("FireUp/Color1", typeof(Material)) as Material;

         c2 = Resources.Load("FireUp/Color2", typeof(Material)) as Material;

      c3 = Resources.Load("FireUp/Color3", typeof(Material)) as Material;
       c4 = Resources.Load("FireUp/Color4", typeof(Material)) as Material;}*/

        // (User_FireUp.Instance.getcurrentDifficult()+" -- "+ ((SpawnBox.countOfSpawnedLines / 3) * User_FireUp.Instance.getcurrentDifficult())));

        float difficult = User_FireUp.Instance.getcurrentDifficult();
        float range = ((SpawnBox.countOfSpawnedLines / 5) * difficult) - difficult;
        float percent = (pointNumber - difficult) / range;
       
       // Debug.Log("@@@ "+ difficult+ "--"  + ((SpawnBox.countOfSpawnedLines / 3) * difficult) +"  %% "+percent);
        if ((pointNumber == 1 ||percent < 0.25) && rendererMain.material != c1)
        {
            rendererMain.material = c1;
            colorIndex = 1;
            //gameObject.GetComponent<Renderer>().material.color = Color.blue;
        }
        else if (percent <  0.5 && rendererMain.material != c2)
        {
            rendererMain.material = c2;
            colorIndex = 2;
            //gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (percent < 0.75 && rendererMain.material != c4)
        {
            rendererMain.material = c3;
            colorIndex = 3;
            //gameObject.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (rendererMain.material != c4)
        {
            rendererMain.material = c4;
            colorIndex = 4;
            //gameObject.GetComponent<Renderer>().material.color = Color.green;
        }
        if (first)
        {
            colorIndexSave = colorIndex;
            first = false;
        }
    }
}
