﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public GameObject ball_0;
    public GameObject ball_1;
    public GameObject ball_2;
    //private AudioClip audio;
    //public GameObject ball;
    //float timeToBall;
    Vector3 offset;
    Vector3 targetPos;
    Vector3 TempPos;
    
    Vector3 standardDirection;
    Vector3 bonusDirectionL;
    Vector3 bonusDirectionR;
    Vector3 tempX;

    public float endRightMove;
    private GameObject spawnBallplace;
    float nextShoot;
    float timeSpawnBall;
    int countLineToMiniGame = 0;
    float operatorZnak = 1.5f;
    bool canDestroyPlayer;
    float timeToDestroyPlayer;
    float countToDestroyPlayer;
    List<GameObject> placeSpanwBall;
    private float speedMove = 1.5f;
    public GameObject effectBox;
    void Start()
    {

        //audio = (AudioClip)Resources.Load("FireUp/pif");
        //GetComponent<AudioSource>().playOnAwake = false;
        //GetComponent<AudioSource>().clip = audio;
        //GetComponent<AudioSource>().volume = 0.3f;
        endRightMove = 2.65f;
        canDestroyPlayer = true;
        timeToDestroyPlayer = 1f;
        countToDestroyPlayer = 0;
        targetPos = new Vector3(4, transform.position.y, transform.position.z);
        standardDirection = new Vector3(0, 0, 10);
        bonusDirectionL = new Vector3(-2, 0, 10);
        bonusDirectionR = new Vector3(2, 0, 10);
        nextShoot = 0f;
        setBallTime(1);
        Debug.Log(spawnBallplace + "**********************************kupa11111111111111*:"+ User_FireUp.Instance.countShotPlayer);
        spawnBallplace = transform.Find("Gun/Gun_" + User_FireUp.Instance.countShotPlayer).gameObject;
        Debug.Log(spawnBallplace + "**********************************kupa*");
    }

    float setBallTime(int multiply) {
        float lvl = User_FireUp.Instance.speedLevel - 1;
        if (lvl <= 20)
        {
            timeSpawnBall = 0.4f - (0.0125f * lvl);
        }
        else
        {
            timeSpawnBall = 0.4f - ((0.025f * 10f) + ((lvl - 10f) * 0.0088f)* multiply);
        }
        //Debug.Log("timeSpawnBall" + timeSpawnBall);
        if (timeSpawnBall < 0.0f)
            timeSpawnBall = 0.0f;
        
        return timeSpawnBall;
    }

    void Update()
    {

        if(!GameManager_FireUp.isGameOver && Time.time > nextShoot)
        {
            nextShoot = Time.time + timeSpawnBall;
            if (BonusManager.getbonusVector())
            {
                SpawnBall(bonusDirectionR);
                SpawnBall(bonusDirectionL);
            }
            else
            {
                SpawnBall(standardDirection);
                //Debug.Log("spawn ball");

                if (BonusManager.getbonusDamageBall())
                    setBallTime(4);
                else
                    setBallTime(1);

            }
        }
        if (GameManager_FireUp.isGameOver && canDestroyPlayer)
        {
            //countToDestroyPlayer += Time.deltaTime;
            //if (timeToDestroyPlayer <= countToDestroyPlayer)
            //{
                Destroy(gameObject);
                //GameObject effectBox = Resources.Load("FireUp/BoxEffect", typeof(GameObject)) as GameObject;
                effectBox.GetComponent<Renderer>().material = gameObject.transform.GetComponent<Renderer>().material;//Resources.Load("FireUp/ColorBoxEffect5", typeof(Material)) as Material;
                Instantiate(effectBox, transform.position, Quaternion.identity);
                canDestroyPlayer = false;
                //countToDestroyPlayer = 0;
            //}
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (!GameManager_FireUp.isGameOver)
            {
                //InvokeRepeating("NewSpawn", 0, timeSpawnBall);
                TempPos = transform.position;
                float distance = transform.position.z - Camera.main.transform.position.z;
                targetPos = new Vector3(Input.mousePosition.x, 0, distance);
                targetPos = Camera.main.ScreenToWorldPoint(targetPos);
                Vector3 followXonly = new Vector3(targetPos.x * speedMove, transform.position.y, transform.position.z);
                tempX = followXonly - TempPos;

            }

        }
        
            if (Input.GetMouseButton(0) &&  !GameManager_FireUp.isGameOver)
            {
                move();
            }
        
        
        //Debug.Log ("player up up end");
    }

    /*IEnumerator NewSpawn()
    {
        
        //yield return new WaitForSeconds(timeSpawnBall);
        while (!GameManager.isGameOver)
        {
            if (BonusManager.bonusVector)
            {
                SpawnBall(bonusDirectionR);
                SpawnBall(bonusDirectionL);

            }
            else
            {
                SpawnBall(standardDirection);

                if (BonusManager.bonusSpeedBall)
                {
                    SpawnBall(standardDirection);
                }
            }
            // shoot stuff
            Debug.Log(Time.time);
            yield return new WaitForSeconds(timeSpawnBall);
        }
    }*/
   /*public void NewSpawn()
       
    {
            if (BonusManager.getbonusVector())
            {
                SpawnBall(bonusDirectionR);
                SpawnBall(bonusDirectionL);

            }
            else
            {
                SpawnBall(standardDirection);

                if (BonusManager.getbonusSpeedBall())
                {
                    SpawnBall(standardDirection);
                }
            }
            // shoot stuff
            //Debug.Log(Time.time);
         
    }*/
    /*public void spawnBallFromBox()
    {
        if (!GameManager.isGameOver)
        {
            if (BonusManager.bonusVector)
            {
                SpawnBall(bonusDirectionR);
                SpawnBall(bonusDirectionL);

            }
            else
            {
                SpawnBall(standardDirection);
                if (BonusManager.bonusSpeedBall)
                {
                    SpawnBall(standardDirection);
                }
            }
        }
    }*/
    private void SpawnBall(Vector3 temp)
    {
        if (User.Instance.volume)
            GetComponent<AudioSource>().Play();
        ////float tempZ = spawnBallplace.transform.position;//0.35f;
        Debug.Log("spawn ball");
        foreach (Transform placeSpawn in spawnBallplace.GetComponentInChildren<Transform>())
        {
            //Debug.Log(placeSpawn.position);
            MakeInstanteBall(placeSpawn.transform.position, temp);
        }

        /*if (transform.name == "Player_0(Clone)")
        {
            MakeInstanteBall(spawnBallplace.transform.position, temp);//MakeInstanteBall(new Vector3(transform.position.x, transform.position.y, transform.position.z + tempZ), temp);
        }
        else if (transform.name == "Player_1(Clone)")
        {
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x - 0.35f, spawnBallplace.transform.position.y, spawnBallplace.transform.position.z), temp);
            //MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x - 0.18f, spawnBallplace.transform.position.y - 0.13f, spawnBallplace.transform.position.z), temp);
        }
        else if (transform.name == "Player_2(Clone)")
        {

            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x + 0.18f, spawnBallplace.transform.position.y - 0.13f, spawnBallplace.transform.position.z), temp);
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x - 0.18f, spawnBallplace.transform.position.y - 0.13f, spawnBallplace.transform.position.z), temp);
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x, spawnBallplace.transform.position.y, spawnBallplace.transform.position.z), temp);
        }
            /*MakeBall(new Vector3(transform.position.x + 0.18f, transform.position.y + 0.13f, transform.position.z + 0.15f), temp);
            MakeBall(new Vector3(transform.position.x - 0.18f, transform.position.y + 0.13f, transform.position.z + 0.15f), temp);
            MakeBall(new Vector3(transform.position.x + 0.18f, transform.position.y - 0.13f, transform.position.z + 0.15f), temp);
            MakeBall(new Vector3(transform.position.x - 0.18f, transform.position.y - 0.13f, transform.position.z + 0.15f), temp);
        }
        else if (transform.name == "Player_3(Clone)")
        {
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x, spawnBallplace.transform.position.y, spawnBallplace.transform.position.z), temp);
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x + 0.18f, spawnBallplace.transform.position.y + 0.13f, spawnBallplace.transform.position.z), temp);
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x - 0.18f, spawnBallplace.transform.position.y + 0.13f, spawnBallplace.transform.position.z), temp);
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x + 0.18f, spawnBallplace.transform.position.y - 0.13f, spawnBallplace.transform.position.z), temp);
            MakeInstanteBall(new Vector3(spawnBallplace.transform.position.x - 0.18f, spawnBallplace.transform.position.y - 0.13f, spawnBallplace.transform.position.z), temp);
        }*/
        //Debug.Log("spawn ball end");

        //Debug.Log(temp.normalized);

    }
    void move()
    {
        //Debug.Log("move player");
        float distance = transform.position.z - Camera.main.transform.position.z;
        targetPos = new Vector3(Input.mousePosition.x, 0, distance);
        targetPos = Camera.main.ScreenToWorldPoint(targetPos);
        Vector3 followXonly = new Vector3(targetPos.x * speedMove, transform.position.y, transform.position.z);
        transform.position = followXonly - tempX;
        if (transform.position.x < -0.5f)
        {
            transform.position = new Vector3(-0.5f, 0.2f, 0);

        }
        else if (transform.position.x > endRightMove)
        {
            transform.position = new Vector3(endRightMove, 0.2f, 0);

        }
        //Debug.Log("move player end");
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("palyer gameov col");
        if (collision.transform.tag.Equals(BoxScript.sBox) || collision.transform.tag.Equals(BoxScript.sMoving_Box) || collision.transform.tag.Equals(BoxScript.sDownUp_Box)) //WsyztkiePudelka
        {
            //Debug.Log("box coll");
            GameManager_FireUp.isGameOver = true;


        }
        //Debug.Log("palyer gameov col end");
    }

    /*private void MakeBall(Vector3 tempPosition, Vector3 tempV)
    {
        GameObject prefab = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        prefab.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
        prefab.AddComponent<Rigidbody>();
        prefab.tag = "Ball";
        prefab.AddComponent<BallScriptUp>();
        prefab.name = "Ball";
        prefab.GetComponent<Rigidbody>().useGravity = false;
        Destroy(prefab.GetComponent<SphereCollider>());
       //prefab.GetComponent<Renderer>().material.color = new Color(62, 255, 0, 255);
        prefab.GetComponent<Renderer>().material = Resources.Load("ColorBall", typeof(Material)) as Material;
        prefab.transform.position = tempPosition;
        prefab.GetComponent<Rigidbody>().velocity = 10 * (tempV.normalized);
    }*/
    //static GameObject ball;// = Resources.Load("FireUp/" + User_FireUp.Instance.nameBall, typeof(GameObject)) as GameObject;
    void MakeInstanteBall(Vector3 tempPositon, Vector3 tempV)
    {
        //Debug.Log("make inst ball");
        /*if (ball == null) {
            ball = Resources.Load("FireUp/" + User_FireUp.Instance.nameBall, typeof(GameObject)) as GameObject;
        }*/
        GameObject ball = ball_0;
        if (User_FireUp.Instance.nameBall == ball_0.transform.name)
            ball = ball_0;
        else if (User_FireUp.Instance.nameBall == ball_1.transform.name)
            ball = ball_1;
        else if (User_FireUp.Instance.nameBall == ball_2.transform.name)
            ball = ball_2;

        GameObject prefab = (GameObject)Instantiate(ball);
        prefab.GetComponent<Renderer>().material.color = User_FireUp.Instance.GetColor(User_FireUp.Instance.colorBall);
        prefab.transform.position = tempPositon;
        prefab.GetComponent<Rigidbody>().velocity = 10 * (tempV.normalized);
       // Debug.Log("make inst ball end");
    }
   
}
