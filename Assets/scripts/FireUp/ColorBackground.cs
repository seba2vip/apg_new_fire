﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBackground : MonoBehaviour
{
    public Material colorBackground;
    private float speed =0.0f;
    private float startTime;
    private Color temp;
    Color start,end;
     float timeToStep=0.001f;
     float every =2;
    private void Start()
    {
        Color temp = new Color(255, 0, 0, 255);
        colorBackground.SetColor("_Color", temp);
        startTime = Time.time;
        temp = Color.red;
        start = Color.red;
        end = Color.magenta;
        
    }
    private void Update()
    {
       

        if (speed < every)
        {
            temp = Color.Lerp(start, end, speed);
            speed += timeToStep;
            colorBackground.SetColor("_Color", temp);
        }else
        {
            speed = 0;
            if (temp == Color.red)
            {
               // Debug.Log("red");
                start = Color.red;
                end = Color.magenta;
            }
            else if (temp == Color.magenta)
            {
               // Debug.Log("magenta");
                start = Color.magenta;
                end = Color.blue;
            }
            else if (temp == Color.blue)
            {
               // Debug.Log("blue");
                start = Color.blue;
                end = Color.cyan;
            }
            else if (temp == Color.cyan)
            {
               // Debug.Log("cyan");
                start = Color.cyan;
                end = Color.green;
            }
            else if (temp == Color.green)
            {
               // Debug.Log("green");
                start = Color.green;
                end = Color.yellow;
            }
            else if (temp == Color.yellow)
            {
               // Debug.Log("yellow");
                start = Color.yellow;
                end = Color.red;
            }
        }
    }
}
