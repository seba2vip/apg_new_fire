﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNewBallShop : MonoBehaviour
{
    public Transform[] place;
    private void Start()
    {

        NewBall();
    }

    private void OnEnable()
    {
        NewBall();
    }
    public void NewBall()
    {
        foreach (Transform placeSpawn in transform.GetComponentInChildren<Transform>())
        {
            foreach (Transform ps in placeSpawn.GetComponentInChildren<Transform>())
            {
               // placeSpawn.GetComponent<MeshRenderer>()
               Destroy(ps.gameObject);
            }

        }
        foreach (Transform t in place)
        {
            Debug.Log("spawn static ball");
               MakeInstanteBall(t.transform.position, t);
        }
    }
    void MakeInstanteBall(Vector3 tempPositon, Transform t)
    {
        GameObject prefab = (GameObject)Instantiate(Resources.Load("FireUp/" + User_FireUp.Instance.nameBall, typeof(GameObject)) as GameObject);
        prefab.GetComponent<BallScript>().enabled = false;
        prefab.transform.tag ="Ball_Gun";
        prefab.GetComponent<Renderer>().material.color = User_FireUp.Instance.GetColor(User_FireUp.Instance.colorBall);
        prefab.transform.position = tempPositon;
        prefab.transform.SetParent(t);
        prefab.GetComponent<Rigidbody>().isKinematic = true;
    }
}
