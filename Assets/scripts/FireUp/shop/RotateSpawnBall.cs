﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSpawnBall : MonoBehaviour {

	private Vector3 standardDirection;
	public static RotateSpawnBall instance;
	float timeToBall;
	float timeSpawnBall;
	float x,z;
	bool isNew;
	//public float timeRotation;
	private static string tempName;
	private static Vector3 tempDir;


	// Use this for initialization
	void Start () {
		instance = this;
		tempName = MovePlayers.tempPlayer.name;
		//Debug.Log ("start rotate");
		isNew = false;
		//timeRotation = 10;
		x = 0;
		z = 10;
		standardDirection = new Vector3(x, 0, z);
		tempDir = standardDirection;
		timeToBall = 0.1f;
		timeSpawnBall = timeToBall;
        
	}

    private void OnDisable()
    {
        isNew = true;
    }

    void Update () {
        if (MovePlayers.tempPlayer.name != tempName && !isNew)
            isNew = true;

		if (isNew)
        {

            Debug.Log("reset kierunku");
			tempDir = new Vector3(0, 0, 10);
			GameObject[] spawnBall = GameObject.FindGameObjectsWithTag ("SpawnBallT");
			foreach (GameObject current in spawnBall) {
				current.GetComponent<RotateSpawnBall>().standardDirection = tempDir;
			}
			tempName = MovePlayers.tempPlayer.name;
            isNew = false;
		}
		//standardDirection = Quaternion.Euler(0, 1, 0) * standardDirection;
        standardDirection = (transform.parent.parent.parent.position - transform.position);
        if (timeToBall < 0)
		{

			SpawnBall(-standardDirection);
			timeToBall = timeSpawnBall;
		}

		timeToBall -= Time.deltaTime;


	}
	private void SpawnBall(Vector3 temp)
	{
		MakeInstanteBall (new Vector3 (transform.position.x, transform.position.y, transform.position.z), temp);
       
        /*if (MovePlayers.tempPlayer.name == "Player_0") {
			MakeInstanteBall (new Vector3 (transform.position.x, transform.position.y, transform.position.z), temp);
		} else if (MovePlayers.tempPlayer.name == "Player_1") {
			MakeInstanteBall (new Vector3 (transform.position.x, transform.position.y, transform.position.z), temp);
			MakeInstanteBall (new Vector3 (transform.position.x, transform.position.y, transform.position.z), temp);
		} else if (MovePlayers.tempPlayer.name == "Player_2") {
		}else if (MovePlayers.tempPlayer.name == "Player_3") {
		}*/


    }
	void MakeInstanteBall(Vector3 tempPositon, Vector3 tempV)
	{
		//Debug.Log ("make inst ball");
		GameObject prefab = (GameObject)Instantiate(Resources.Load("FireUp/"+ User_FireUp.Instance.nameBall, typeof(GameObject)) as GameObject);
        prefab.GetComponent<Renderer>().material.color = User_FireUp.Instance.GetColor(User_FireUp.Instance.colorBall);
		prefab.transform.position = tempPositon;
		prefab.GetComponent<Rigidbody>().velocity = 10 * ( tempV.normalized);
		//Debug.Log ("make inst ball end");
	}
}
