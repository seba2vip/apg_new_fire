﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeColor : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.GetComponent<Renderer>().material.color = transform.parent.GetComponent<Renderer>().material.color;
        //Debug.Log("zmian koloru: " + transform.parent.parent.name);
    }

    // Update is called once per frame
    void Update () {
        if(SceneManager.GetActiveScene().name =="Shop_FireUp")
        transform.GetComponent<Renderer>().material.color = transform.parent.GetComponent<Renderer>().material.color;
        //Debug.Log("zmian koloru: " + transform.parent.parent.name);
	}
}
