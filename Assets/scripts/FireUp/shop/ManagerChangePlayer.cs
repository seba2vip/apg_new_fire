﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ManagerChangePlayer : MonoBehaviour {

    public GameObject playerSelect;
    public GameObject ballSelect;
    //public GameObject[] ballColorToCreat;
    public GameObject playerList;
    public GameObject ballList;
    public Text buttonToBuyDown;
    public Text buttonToBuyUp;
    public Text score;
    public static whichMoveType whichMove;
    private ThingsToBuy thingsToBuy;
    private void Awake()
    {
        User_FireUp.Instance.loadUser();
        MovePlayers.tempPlayer = GameObject.Find("Player_00");
        //MoveColor.temoColor = GameObject.Find("Color_0");
    }
    void Start() {
        ADManager.instance.destroyBanner();
        playerSelect.SetActive(true);
        ballSelect.SetActive(false);
        whichMove = whichMoveType.player;
        //MakeBallColor();
        thingsToBuy = ThingsToBuy.player;
        buttonToBuyUp.text = "text";
        score.text = User_FireUp.Instance.overallScore.ToString();
    }

    void Update()
    {
        refreshTextButton();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitButton();
        }
    }
    void refreshTextButton()
    {
        ButtonDownChange();
        ButtonUpChange();
    }
    public void ExitButton()
    {
        SceneManager.LoadScene("Game_FireUp");
    }

    public void DragButtonPlayer()
    {
        whichMove = whichMoveType.player;
    }
    public void DragButtonColor()
    {
        whichMove = whichMoveType.color;
    }

    public void PlayerShowButton()
    {
        thingsToBuy = ThingsToBuy.player;
        playerSelect.SetActive(true);
        ballSelect.SetActive(false);
    }
    public void BallShowButton()
    {
        GameObject[] pl = GameObject.FindGameObjectsWithTag("Shop_player");
        //Debug.Log("dsadsaddd :" + pl.Length);
        foreach (GameObject p in pl)
        {
            //Debug.Log(p.name);
            p.transform.eulerAngles = new Vector3(-10, 0, 0);
            p.transform.Rotate(new Vector3(-14, 0, 0));
        }
        playerSelect.SetActive(false);
        ballSelect.SetActive(true);
        //SetColorBall();
        thingsToBuy = ThingsToBuy.ball;

        
    }
    public void ButtonToBuyDown() {
        Debug.Log("button buy down");

        if (thingsToBuy == ThingsToBuy.player) {
            if (getPlayer().type == User_FireUp.TypeOwn.ToBuy) {
                if (getPlayer().cost < User_FireUp.Instance.overallScore) {
                    getPlayer().type = User_FireUp.TypeOwn.Bought;
                    User_FireUp.Instance.overallScore -= getPlayer().cost;
                    User_FireUp.Instance.saveUser();
                }
            } else if (getPlayer().type == User_FireUp.TypeOwn.Bought) {
                ChoosePlayer();
                User_FireUp.Instance.saveUser();
            }
        } else if(thingsToBuy == ThingsToBuy.ball)
        {
            if (getBall().type == User_FireUp.TypeOwn.ToBuy) {
                if (getBall().cost < User_FireUp.Instance.overallScore) {
                    getBall().type = User_FireUp.TypeOwn.Bought;
                    User_FireUp.Instance.overallScore -= getBall().cost;
                    User_FireUp.Instance.saveUser();
                }
            } else if (getBall().type == User_FireUp.TypeOwn.Bought) {
                ChoosePlayer();
                User_FireUp.Instance.saveUser();
            }
        }
        score.text = User_FireUp.Instance.overallScore.ToString();

    }
    public void ButtonToBuyUp() {
        Debug.Log("button buy up");
        if (thingsToBuy == ThingsToBuy.player) {
           if (GetPlayerColor().type == User_FireUp.TypeOwn.ToBuy && getPlayer().type != User_FireUp.TypeOwn.ToBuy) {
                if (GetPlayerColor().cost < User_FireUp.Instance.overallScore) {
                    GetPlayerColor().type = User_FireUp.TypeOwn.Bought;
                    User_FireUp.Instance.overallScore -= GetPlayerColor().cost;
                    User_FireUp.Instance.saveUser();
                }
            } else if (GetPlayerColor().type == User_FireUp.TypeOwn.Bought) {
                ChoosePlayerColor();
                User_FireUp.Instance.saveUser();
            }
        } else if(thingsToBuy == ThingsToBuy.ball && MoveColor.temoColor.tag.Equals("Shop_color"))
        {
            if (GetBallColor().type == User_FireUp.TypeOwn.ToBuy && getBall().type != User_FireUp.TypeOwn.ToBuy) {
                if (GetBallColor().cost < User_FireUp.Instance.overallScore) {
                    GetBallColor().type = User_FireUp.TypeOwn.Bought;
                    User_FireUp.Instance.overallScore -= GetBallColor().cost;
                    User_FireUp.Instance.saveUser();
                }
            } else if (GetBallColor().type == User_FireUp.TypeOwn.Bought) {
                ChoosePlayerColor();
                User_FireUp.Instance.saveUser();
            }
        }
        score.text = User_FireUp.Instance.overallScore.ToString();
    }

    void ButtonDownChange() {
        if (thingsToBuy == ThingsToBuy.player) {
            if (getPlayer().type == User_FireUp.TypeOwn.ToBuy) {
                buttonToBuyDown.text = "Buy\n " + getPlayer().cost.ToString();
            } else if (getPlayer().type == User_FireUp.TypeOwn.Bought) {
                buttonToBuyDown.text = "Choose";
            } else {

                buttonToBuyDown.text = "Selected";
            }
        } else if(thingsToBuy == ThingsToBuy.ball)
        {
            if (getBall().type == User_FireUp.TypeOwn.ToBuy) {
                buttonToBuyDown.text = "Buy\n " + getBall().cost.ToString();
            } else if (getBall().type == User_FireUp.TypeOwn.Bought) {
                buttonToBuyDown.text = "Choose";
            } else {
                buttonToBuyDown.text = "Selected";
            }
        }

    }
    void ButtonUpChange() {
        if (thingsToBuy == ThingsToBuy.player) {
            if (getPlayer().type != User_FireUp.TypeOwn.ToBuy)
            {
                if (GetPlayerColor().type == User_FireUp.TypeOwn.ToBuy)
                {
                    buttonToBuyUp.text = "Buy\n " + GetPlayerColor().cost.ToString();
                }
                else if (GetPlayerColor().type == User_FireUp.TypeOwn.Bought)
                {
                    buttonToBuyUp.text = "Choose";
                }
                else
                {
                    buttonToBuyUp.text = "Selected";
                }
            }
            else
            {
                buttonToBuyUp.text = "Buy Character";
            }
        } else if(thingsToBuy == ThingsToBuy.ball && MoveColor.temoColor.tag.Equals("Shop_color"))
        {
            if (getBall().type != User_FireUp.TypeOwn.ToBuy )
            {
                if (GetBallColor().type == User_FireUp.TypeOwn.ToBuy ) {
                    buttonToBuyUp.text = "Buy\n " + GetBallColor().cost.ToString();
                } else if (GetBallColor().type == User_FireUp.TypeOwn.Bought) {
                    buttonToBuyUp.text = "Choose";
                } else {
                    buttonToBuyUp.text = "Selected";
                }
            }
            else
            {
                buttonToBuyUp.text = "Buy Ball";
            }
        }

    }

    void ChoosePlayer() {
        if (thingsToBuy == ThingsToBuy.player) {
            foreach(User_FireUp.CharacterToBuy pl in User_FireUp.Instance.listPlayer)
            {
                if(pl.type == User_FireUp.TypeOwn.Chosen)
                {
                    pl.type = User_FireUp.TypeOwn.Bought;
                    getPlayer().type = User_FireUp.TypeOwn.Chosen;
                    User_FireUp.Instance.namePlayer = getPlayer().name;
                    foreach(User_FireUp.ShootToBuy col in getPlayer().listCount)
                    {
                        if(col.type == User_FireUp.TypeOwn.Chosen)
                            User_FireUp.Instance.countShotPlayer = col.countShot;
                    } 
                }
            }
        } else if(thingsToBuy == ThingsToBuy.ball && MoveColor.temoColor.tag.Equals("Shop_color")) {
            foreach (User_FireUp.ItemToBuy bal in User_FireUp.Instance.listBall)
            {
                if (bal.type == User_FireUp.TypeOwn.Chosen)
                {
                    bal.type = User_FireUp.TypeOwn.Bought;
                    getBall().type = User_FireUp.TypeOwn.Chosen;
                    //Debug.Log("name ball: " + getBall().name);
                    User_FireUp.Instance.nameBall = getBall().name;
                    foreach (User_FireUp.ColorToBuy col in getBall().listColor)
                    {
                        if (col.type == User_FireUp.TypeOwn.Chosen)
                            User_FireUp.Instance.colorBall = col.color;
                    }
                }
            }
        }
    }
    void ChoosePlayerColor() {
        if (thingsToBuy == ThingsToBuy.player) {
            foreach (User_FireUp.ShootToBuy pl in getPlayer().listCount)
            {
                if (pl.type == User_FireUp.TypeOwn.Chosen)
                {
                    pl.type = User_FireUp.TypeOwn.Bought;
                    GetPlayerColor().type = User_FireUp.TypeOwn.Chosen;
                    User_FireUp.Instance.countShotPlayer = GetPlayerColor().countShot;
                }
            }
        } else if(thingsToBuy == ThingsToBuy.ball && MoveColor.temoColor.tag.Equals("Shop_color"))
        {
            foreach (User_FireUp.ColorToBuy pl in getBall().listColor)
            {
                if (pl.type == User_FireUp.TypeOwn.Chosen)
                {
                    pl.type = User_FireUp.TypeOwn.Bought;
                    GetBallColor().type = User_FireUp.TypeOwn.Chosen;
                    User_FireUp.Instance.colorBall = GetBallColor().color;
                }
            }
        }
    }

    public User_FireUp.CharacterToBuy getPlayer()
    {
        foreach (User_FireUp.CharacterToBuy item in User_FireUp.Instance.listPlayer)
        {
            if (item.name == MovePlayers.tempPlayer.name)
            {
                return item;
            }
        }
        return User_FireUp.Instance.listPlayer[0]; // do poprawy
    }
    public User_FireUp.ItemToBuy getBall()
    {
        foreach (User_FireUp.ItemToBuy item in User_FireUp.Instance.listBall)
        {
            if (item.name == MovePlayers.tempPlayer.name)
            {
                return item;
            }
        }
        return User_FireUp.Instance.listBall[0]; // do poprawy

    }
    public User_FireUp.ShootToBuy GetPlayerColor()
    {
        foreach (User_FireUp.ShootToBuy item in getPlayer().listCount)
        {
            if (item.countShot == MoveColor.temoColor.transform.childCount)
            {
                return item;
            }
        }
        return User_FireUp.Instance.listPlayer[0].listCount[0]; // do poprawy
    }
    public User_FireUp.ColorToBuy GetBallColor()
    {
        foreach (User_FireUp.ColorToBuy item in getBall().listColor)
        {
            
            if (User_FireUp.Instance.GetColor(item.color) == MoveColor.temoColor.GetComponent<Renderer>().material.color)
            {
                return item;
            }
        }
        return User_FireUp.Instance.listBall[0].listColor[0]; // do poprawy
    }
    /*public void MakeBallColor()
    {
        GameObject[] deleteOldBallColor = GameObject.FindGameObjectsWithTag("Shop_color");
        //Debug.Log("liczba kulek kolor: " + deleteOldBallColor.Length);
        bool firstTime = true;
        if (deleteOldBallColor.Length > 0)
            firstTime = false;
        foreach (GameObject go in deleteOldBallColor)
            Destroy(go.gameObject);
        int x = 0;
        Debug.Log("liczba gun: " + getPlayer().listCount.Count);
        foreach(User_FireUp.ShootToBuy col in getPlayer().listCount)
        {
            GameObject.Find("ColorPlayers").transform.position = new Vector3(0, 0, 0);
            GameObject prefab = (GameObject)Instantiate(ballColorToCreat[x]);
            prefab.transform.parent = GameObject.Find("ColorPlayers").transform;
            //prefab.name = "Color_" + x;
            //if(Color.black != User_FireUp.Instance.GetColor(col.color))
            {
              //  prefab.GetComponent<Renderer>().material.color = User_FireUp.Instance.GetColor(col.color);
            }
            //else
            {
                //.GetComponent<Renderer>().material = col.material;wyjebalem
                //Debug.Log("nazawa shadara" + prefab.GetComponent<Renderer>().sharedMaterial.shader.name);
                
            }
            //prefab.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(13, 1));
            if(thingsToBuy == ThingsToBuy.player)
                prefab.transform.position = new Vector3((x * 2.5f) , 3, 0);
            else
                prefab.transform.position = new Vector3((x * 2.5f) , 3, 0);
            x++;
        }
        int i = 0;
        /*if (!firstTime)
            i = GetIndexBallColorPlayerCurrently();
        else
        {
            if (thingsToBuy == ThingsToBuy.player)
                i = GetIndexBallColorPlayer();
            else
                i = GetIndexBallColorBall();
        }
        GameObject.Find("ColorPlayers").transform.position = new Vector3(-2.5f * i, transform.position.y, transform.position.z);

    }*/
    /*  public int GetIndexBallColorPlayer()
      {
          int i = 0;
          foreach (User_FireUp.ColorToBuy t in getPlayer().listColor)
          {
              if(User_FireUp.Instance.colorPlayer == t.color)
              {
                  i = getPlayer().listColor.IndexOf(t);
            //      Debug.Log(i);
              }
          }
          return i;
      }*/
    /*public int GetIndexBallColorPlayerCurrently()
    {
        int i = 0;
        foreach (User_FireUp.ColorToBuy t in getPlayer().listColor)
        {
            if (MoveColor.temoColor.GetComponent<Renderer>().material.color == User_FireUp.Instance.GetColor(t.color))
            {
                i = getPlayer().listColor.IndexOf(t);
                //      Debug.Log(i);
            }
        }
        return i;
    }*/
    public int GetIndexBallColorBall()
    {
        int i = 0;
        foreach (User_FireUp.ColorToBuy t in getBall().listColor)
        {
            if (User_FireUp.Instance.colorBall == t.color)
            {
                i = getBall().listColor.IndexOf(t);
                //      Debug.Log(i);
            }
        }
        return i;
    }
    public enum whichMoveType
    {
        player, color
    }
    public enum ThingsToBuy
    {
        player, ball,other
    }
}
