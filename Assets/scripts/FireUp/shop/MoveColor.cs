﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveColor : MonoBehaviour {

    private Vector3 tempPos;
    private Vector3 targetPos;
    private Vector3 tempX;
    public static GameObject temoColor;
    ManagerChangePlayer mcp;
    private float hight = -3.5f;
    float length;
    // Use this for initialization
    void Start()
    {
        
        mcp = GameObject.Find("Manager").GetComponent<ManagerChangePlayer>();
        if (transform.name == "ColorPlayers")
        {
            //int i = mcp.GetIndexBallColorPlayer();
            
            //transform.position = new Vector3(-2.5f * i, transform.position.y, transform.position.z);
        }
        else if (transform.name == "ColorBall")
        {
            int i = mcp.GetIndexBallColorBall();
            transform.position = new Vector3(-2.5f * i, transform.position.y, transform.position.z);
        }
    }

    void Update()
    {
        //Debug.Log(transform.position);
        if (transform.name == "ColorPlayers")
            length = (mcp.getPlayer().listCount.Count - 1) * 2.5f;
        else
            length = (mcp.getBall().listColor.Count - 1) * 2.5f;


        //Debug.Log ("move color up");
        if (Input.GetMouseButtonDown(0))
        {
            tempPos = transform.position;
            float distance = transform.position.z - Camera.main.transform.position.z;
            targetPos = new Vector3(Input.mousePosition.x, 0, distance);
            targetPos = Camera.main.ScreenToWorldPoint(targetPos);
            Vector3 followXonly = new Vector3(targetPos.x, transform.position.y, transform.position.z);
            tempX = followXonly - tempPos;
        }
        if (Input.GetMouseButton(0))
        {
            if ( ManagerChangePlayer.whichMove != ManagerChangePlayer.whichMoveType.player)
            {
                move();
            }
        }
        if (!(transform.position.x % 2.5f == 0))
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(PositionChange(transform.position.x), transform.position.y, transform.position.z), Time.deltaTime * 2);
        }
        TransformZ(transform.position);
		//Debug.Log ("move color up end");
    }
    void move()
    {
		//Debug.Log ("move");
        float distance = transform.position.z - Camera.main.transform.position.z;
        targetPos = new Vector3(Input.mousePosition.x, hight, distance);
        targetPos = Camera.main.ScreenToWorldPoint(targetPos);
        Vector3 followXonly = new Vector3(targetPos.x, transform.position.y, transform.position.z);
        transform.position = followXonly - tempX;

        if (transform.position.x <= -length)
        {
            transform.position = new Vector3(-length, hight, 0);

        }
        else if (transform.position.x >= 0)
        {
            transform.position = new Vector3(0, hight, 0);

        }
		//Debug.Log ("move end");


    }
    float PositionChange(float posX)
    {
		//Debug.Log ("pos change");
        float left = 0;
        float right = 0;
        
        //Debug.Log("length:" + length);
        for (float i = 0; i <= length; i += 2.5f)
        {
            if (-posX >= i && -posX < i + 2.5f)
            {
                left = i;
                right = i + 2.5f;
            }
        }
        float wynikL = -posX - left;
        float wynikR = right + posX;
        if (wynikL < wynikR)
        {
            return -left;
        }
        else
        {
            return -right;
        }
		//Debug.Log ("pos change end");
    }
    void TransformZ(Vector3 tempp)
    {
		//Debug.Log ("tranzform z ");
        float distance = 0;
        float posPlayer = PositionChange(tempp.x);
        //Debug.Log("posPlayer: " + posPlayer);

        float index = -(posPlayer / 2.5f);
        if(transform.name == "ColorPlayers")
        {
           // Debug.Log("index gun:" + index);
            temoColor = GameObject.Find("Gun_" + index);
        }
        else if(transform.name == "ColorBall")
        {
            temoColor = GameObject.Find("ColorBall_" + index);
        }
        
        if (posPlayer == 0)
        {
            distance = -tempp.x - 1.5f;
        }
        else if (posPlayer < 0)
        {
            if (tempp.x > posPlayer)
            {
                distance = -(1.5f + (posPlayer - tempp.x));
            }
            else
            {
                distance = -tempp.x - (-posPlayer + 1.5f);

            }
        }
        float y = distance;
        //Debug.Log("distance:" + distance);
        if (y < 0)
        {
            y = -y;
        }
        y -= 0.5f;
        refreshOtherPosytion();
        if (transform.name == "ColorPlayers")
            temoColor.transform.position = new Vector3(temoColor.transform.position.x,   (y * 2.3f)-2.9f, distance * 6);
        else
            temoColor.transform.position = new Vector3(temoColor.transform.position.x, (y * 2.3f) - 2.9f, (distance * 6) +0.5f);
        if (transform.name == "ColorBall")
            if (MovePlayers.tempPlayer != null && temoColor != null) {
            if (MovePlayers.tempPlayer.GetComponent<Renderer>().material.color != temoColor.GetComponent<Renderer>().material.color)
            {
                MovePlayers.tempPlayer.GetComponent<Renderer>().material.color = temoColor.GetComponent<Renderer>().material.color; 
                //Material tempCol = Resources.Load("Colors_shop/brazowy", typeof(Material)) as Material;
                //Material tempCol = mcp.getPlayer().listColor[1].material;
                /*if (temoColor.GetComponent<Renderer>().material.color == mcp.getPlayer().listColor[1].color)
                {
                    Debug.Log("zmiana_koloru/postacie: "+ GameObject.Find("kurczak_0/Cube").name);
                    GameObject.Find("kurczak_0/Cube").GetComponent<Renderer>().material.color = mcp.getPlayer().listColor[1].color;
                    //GameObject.Find("kurczak_1/Cube").SetActive(true);
                    //GameObject.Find("kurczak_0").SetActive(false);

                }
                else
                {
                    
                }*/
            }
        } 
		//Debug.Log ("tranzform z end");
    }
    void refreshOtherPosytion()
    {
        GameObject[] otherObject;

        if (transform.name == "ColorPlayers")
            otherObject = GameObject.FindGameObjectsWithTag("Shop_Gun");
        else
            otherObject = GameObject.FindGameObjectsWithTag("Shop_color");

        foreach (GameObject go in otherObject)
        {
            if(temoColor != go)
            {
                go.transform.position = new Vector3(go.transform.position.x, hight, -1.924f);
            }
        }  
    }
    /*
    User.ColorToBuy getColor()
    {
        foreach (User.ColorToBuy item in mcp.getPlayer().listColor)
        {
            //Debug.Log("item: " + item.name);
            if (item.color == MovePlayers.tempPlayer.GetComponent<Renderer>().material.color)
            {
                return item;
            }
        }
        return mcp.getPlayer().listColor[0]; // do poprawy
    }*/
}
