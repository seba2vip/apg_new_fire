﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovePlayers : MonoBehaviour {

    Vector3 tempPos;
    Vector3 targetPos;
    Vector3 tempX;
    public static GameObject tempPlayer;
    float index;
    float tempIndex;
	float countModel;
	public GameObject activSpawn;
    private int indexShot=0;
    ManagerChangePlayer mcp;
    private float hight = 0f;
    //RotateSpawnBall rot;
    // Use this for initialization
    void Start () {
        if (transform.name == "Players")
        {
            string s = User_FireUp.Instance.namePlayer.Substring(User_FireUp.Instance.namePlayer.Length - 2);
            int qw = Int32.Parse(s);
            //int qw = (int)char.GetNumericValue(User_FireUp.Instance.namePlayer[User_FireUp.Instance.namePlayer.Length - 1]);
            transform.position = new Vector3(-2.5f * qw, transform.position.y, transform.position.z);
        }
        else
        {
            int qw = (int)char.GetNumericValue(User_FireUp.Instance.nameBall[User_FireUp.Instance.nameBall.Length - 1]);
            transform.position = new Vector3(-2.5f * qw, transform.position.y, transform.position.z);
        }
        mcp = GameObject.Find("Manager").GetComponent<ManagerChangePlayer>();
        //rot = GetComponent<RotateSpawnBall>();
        countModel = 11;
		countModel = (countModel - 1) * 2.5f;
        index = 0;
        tempIndex = 0;
        indexShot = 1;
    }

    void Update()
    {

        //Debug.Log ("move player up");
        if (Input.GetMouseButtonDown(0))
        {
            tempPos = transform.position;
            float distance = transform.position.z - Camera.main.transform.position.z;
            targetPos = new Vector3(Input.mousePosition.x, 0, distance);
            targetPos = Camera.main.ScreenToWorldPoint(targetPos);
            Vector3 followXonly = new Vector3(targetPos.x, transform.position.y, transform.position.z);
            tempX = followXonly - tempPos;
        }
        if (Input.GetMouseButton(0))
        {
            if (ManagerChangePlayer.whichMove == ManagerChangePlayer.whichMoveType.player)
            {
                move();
            }
        }
        if ( !(transform.position.x % 2.5f == 0))
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(PositionChange(transform.position.x), transform.position.y, transform.position.z), Time.deltaTime *2);
        }
        TransformZ(transform.position);
		//Debug.Log ("move player up end");
    }
    void move()
    {
		//Debug.Log ("move player ");
        float distance = transform.position.z - Camera.main.transform.position.z;
        targetPos = new Vector3(Input.mousePosition.x, hight, distance);
        targetPos = Camera.main.ScreenToWorldPoint(targetPos);
        Vector3 followXonly = new Vector3(targetPos.x, transform.position.y, transform.position.z);
        transform.position = followXonly - tempX;

		if (transform.name == "Players") {
			if (transform.position.x <= -countModel)
			{
				transform.position = new Vector3(-countModel, hight, 0);

			}
			else if (transform.position.x >= 0)
			{
				transform.position = new Vector3(0, hight, 0);

			}
		} else if (transform.name == "Ball") {
			if (transform.position.x <= -5)
			{
				transform.position = new Vector3(-5, hight, 0);

			}
			else if (transform.position.x >= 0)
			{
				transform.position = new Vector3(0, hight, 0);

			}
		}



		//Debug.Log ("move player end");

    }
    float PositionChange(float posX)
    {
		//Debug.Log ("player pos change");
        float left = 0;
        float right = 0;
		if (transform.name == "Players") {
			for (float i = 0; i <= countModel; i += 2.5f)
			{
				if (-posX >= i && -posX < i + 2.5f)
				{
					left = i;
					right = i + 2.5f;
				}
			}
		} else if (transform.name == "Ball") {
			for (float i = 0; i <= 5; i += 2.5f)
			{
				if (-posX >= i && -posX < i + 2.5f)
				{
					left = i;
					right = i + 2.5f;
				}
			}
		}

        float wynikL = -posX - left;
        float wynikR = right + posX;
        if (wynikL < wynikR)
        {
            return -left;
        }
        else
        {
            return -right;
        }
		//Debug.Log ("player pos change end");
    }
    void TransformZ(Vector3 tempp)
    {
		//Debug.Log ("player trnas z ");
        float distance = 0;
        float posPlayer = PositionChange(tempp.x);

        tempIndex = index;
        index = -(posPlayer / 2.5f);

        if(transform.name == "Players")
        {
            //bool changeColor = false;
            if (tempIndex != index )
            {
                //Debug.Log(indexShot);
                //GameObject.Find("Player_" + tempIndex).GetComponent<Renderer>().material.color = User_FireUp.Instance.GetColor(mcp.getPlayer().listColor[0].color);
                activSpawn = transform.Find(tempPlayer.name + "/Gun/Gun_"+ MoveColor.temoColor.transform.childCount).gameObject;
                //changeColor = true;
                activSpawn.SetActive (false);
                indexShot = MoveColor.temoColor.transform.childCount;
                //tempPlayer.transform.Rotate(new Vector3(0, 0, 0));
                //tempPlayer.transform.rotation.eulerAngles = new Vector3 (0, 0, 0);
                tempPlayer.transform.eulerAngles = new Vector3 (-10, 0, 0);
				tempPlayer.transform.Rotate(new Vector3(-14, 0, 0));
               // RotateSpawnBall.isNew = true;
				//activSpawn.GetComponent<RotateSpawnBall>().enabled = false;
				//RotateSpawnBall.instance.standardDirection = new Vector3(0, 0, 10);
            }
            //Debug.Log("tempPlayer:" + tempPlayer.name + " / " + tempPlayer.transform.position);
            if(index>=10)
                tempPlayer = GameObject.Find("Player_" + index);
            else
                tempPlayer = GameObject.Find("Player_0" + index);
            /*if (changeColor)
            {
                mcp.MakeBallColor();
                changeColor = false;
            }*/
            ///Debug.Log("tempPlayer:"+tempPlayer.name+" / "+ tempPlayer.transform.position);
            for (int i=1;i<= 5;i++)
            {
                if(i!= MoveColor.temoColor.transform.childCount)
                    transform.Find(tempPlayer.name + "/Gun/Gun_" + i).gameObject.SetActive(false);
            }
            if (tempPlayer.tag.Equals("Shop_player")&& MoveColor.temoColor.transform.childCount>0)
            {
                //Debug.Log("name: " + tempPlayer.name + " count:" + MoveColor.temoColor.transform.childCount);
                activSpawn = transform.Find(tempPlayer.name + "/Gun/Gun_" + MoveColor.temoColor.transform.childCount).gameObject;
                activSpawn.SetActive(true);
            }
			//activSpawn.GetComponent<RotateSpawnBall>().enabled = true;
			//Quaternion rotation = tempPlayer.transform.;
			//tempPlayer.transform.localRotation = RotateSpawnBall.instance.standardDirection;
			tempPlayer.transform.Rotate(new Vector3(0, 1, 0));
			//gameObject.GetComponent<RotateSpawnBall>().standardDirection 

        }
        else if (transform.name == "Ball")
        {
           

			if (tempIndex != index)
            {
                GameObject.Find("Ball_" + tempIndex).GetComponent<Renderer>().material.color = new Color(0, 255, 0, 255);
            }
            tempPlayer = GameObject.Find("Ball_" + index);
        }
        
        if (posPlayer == 0)
        {
            distance = -tempp.x - 1.5f;
        }
        else if (posPlayer < 0)
        {
            if (tempp.x > posPlayer)
            {
                distance = -(1.5f + (posPlayer - tempp.x));
            }
            else
            {
                distance = -tempp.x - (-posPlayer + 1.5f);

            }
        }
        float y = distance;
        //Debug.Log("distance:" + distance);
        if (y < 0)
        {
            y = -y;
        }
        y += 0.5f;
        refreshOtherPosytion();
        //Debug.Log("tempPlayer1:" + tempPlayer.name + " / " + tempPlayer.transform.position);
        if (transform.name == "Players")
            tempPlayer.transform.position = new Vector3(tempPlayer.transform.position.x, 4 - (y * 1.4f) + hight, distance * 6);
        else
            tempPlayer.transform.position = new Vector3(tempPlayer.transform.position.x, 4 - (y * 1.4f) + hight, (distance * 6 )+ 1);


        //Debug.Log("tempPlayer2:" + tempPlayer.name + " / " + tempPlayer.transform.position);
        //Debug.Log ("player trnas z end");
    }
    void refreshOtherPosytion()
    {
        GameObject[] otherObject = GameObject.FindGameObjectsWithTag("Shop_player");
        foreach (GameObject go in otherObject)
        {
            if (tempPlayer != go)
            {
                go.transform.position = new Vector3(go.transform.position.x, 2.85f + hight, -1.75f);
            }
        }
    }

}
