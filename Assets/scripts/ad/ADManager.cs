﻿using GoogleMobileAds;
using GoogleMobileAds.Api;
using GoogleMobileAds.Android.Mediation.AdColony;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api.Mediation.AdColony;
using GoogleMobileAds.Api.Mediation.AppLovin;
using GoogleMobileAds.Api.Mediation.UnityAds;
using GoogleMobileAds.Api.Mediation.Vungle;

public class ADManager : MonoBehaviour
{
#if UNITY_EDITOR
    string APP_ID = "unused";
#elif UNITY_ANDROID
        string APP_ID = "ca-app-pub-1418224687952214~6539984314";
#elif UNITY_IPHONE
        string APP_ID = "ca-app-pub-1418224687952214~5397387134";
#else
        string APP_ID = "unexpected_platform";
#endif
    public static bool isPremium;
    public static ADManager instance;
    //private string APP_ID = "ca-app-pub-1418224687952214~2530427510";

    public BannerView bannerAD;
    private InterstitialAd interstitialAD;
    private RewardBasedVideoAd rewardVideoAd;
    //AdColonyMediationExtras extrasAdColony = new AdColonyMediationExtras();
    private void Awake()
    {
            instance = this;
       
      

        isPremium = User.Instance.isPremium;

        //extrasAdColony.SetShowPrePopup(true);
        //extrasAdColony.SetShowPostPopup(true);
        
        //MobileAds.SetiOSAppPauseOnBackground(true);
        MobileAds.Initialize(APP_ID); //do publikacji
    }

    void Start()
    {
        Debug.Log("admanager***************");

        //
        //RequestInterstitial();
        //RequestVideoAD();
        //Display_Banner();
    }

    public bool ReturnIsPremium()
    {
        return isPremium;
    }
    public void RequestBanner()
    {
        
#if UNITY_EDITOR
        string banner_ID = "unused";
#elif UNITY_ANDROID
        string banner_ID = "ca-app-pub-1418224687952214/1287657636";
#elif UNITY_IPHONE
        string banner_ID = "ca-app-pub-1418224687952214/8869728289";
#else
        string banner_ID = "unexpected_platform";
#endif
        if (bannerAD != null)
        {
            return;
            //bannerAD.Destroy();
        }
        bannerAD = new BannerView(banner_ID, AdSize.SmartBanner, AdPosition.Bottom);
        //AdRequest adRequest = new AdRequest.Builder().Build();
        bannerAD.OnAdLoaded += HandleAdLoaded;
        bannerAD.OnAdFailedToLoad += HandleAdFailedToLoad;
        bannerAD.OnAdOpening += HandleAdOpened;
        bannerAD.OnAdClosed += HandleAdClosed;
        bannerAD.OnAdLeavingApplication += HandleAdLeftApplication;
        loadBAnner();
    }

    private void loadBAnner() {
        AdRequest adRequest = new AdRequest.Builder().AddTestDevice("A99580108C35C670BFFC673C52DE7EB1").AddTestDevice("BE866302C2DB941C84FEAC2E3BD99B90")/*.AddMediationExtras(extrasAdColony)*/.Build();//test

        bannerAD.LoadAd(adRequest);
    }
    public int ReturnSizeBanner()
    {
        return AdSize.Banner.Height;
    }

    public void CreateInterstitial()
    {
#if UNITY_EDITOR
        string interstitial_ID = "unused";
#elif UNITY_ANDROID
        string interstitial_ID = "ca-app-pub-1418224687952214/3883877290";
#elif UNITY_IPHONE
        string interstitial_ID = "ca-app-pub-1418224687952214/1074998743";
#else
        string interstitial_ID = "unexpected_platform";
#endif

        // Clean up interstitial ad before creating a new one.
        if (interstitialAD != null)
        {
            interstitialAD.Destroy();
        }

        // Create an interstitial.
        interstitialAD = new InterstitialAd(interstitial_ID);

        // Register for ad events.
        interstitialAD.OnAdLoaded += HandleInterstitialLoaded;
        interstitialAD.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
        interstitialAD.OnAdOpening += HandleInterstitialOpened;
        interstitialAD.OnAdClosed += HandleInterstitialClosed;
        interstitialAD.OnAdLeavingApplication += HandleInterstitialLeftApplication;
        //AdRequest adRequest = new AdRequest.Builder().Build();
        LoadInterstitial();
    }
    public void LoadInterstitial() {
//        VungleInterstitialMediationExtras extras = new VungleInterstitialMediationExtras();
//#if UNITY_ANDROID
//        extras.SetAllPlacements(new string[] { "interstitial" });
//#elif UNITY_IPHONE
//    extras.SetAllPlacements(new string[] { "IOS_PLACEMENT_1", "IOS_PLACEMENT_2" });
//#endif

        AdRequest adRequest = new AdRequest.Builder().AddTestDevice("A99580108C35C670BFFC673C52DE7EB1")/*.AddMediationExtras(extras)*/.AddTestDevice("BE866302C2DB941C84FEAC2E3BD99B90")/*.AddMediationExtras(extrasAdColony)*/.Build();//test
        Debug.Log("*************************************\n*************************************\n" + SceneManager.GetActiveScene().name + "*************************************\n**************************************");
        interstitialAD.LoadAd(adRequest);
    }



//    public void RequestVideoAD()
//    {
//        // string video_ID = "ca-app-pub-1418224687952214/1826430332";
//#if UNITY_EDITOR
//        string video_ID = "unused";
//#elif UNITY_ANDROID
//        string video_ID = "ca-app-pub-3940256099942544/5224354917";
//#elif UNITY_IPHONE
//        string video_ID = "ca-app-pub-3940256099942544/1712485313";
//#else
//        string video_ID = "unexpected_platform";
//#endif
//        rewardVideoAd = RewardBasedVideoAd.Instance;

    //        rewardVideoAd.OnAdLoaded += HandleRewardBasedVideoLoaded;
    //        rewardVideoAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
    //        rewardVideoAd.OnAdOpening += HandleRewardBasedVideoOpened;
    //        rewardVideoAd.OnAdStarted += HandleRewardBasedVideoStarted;
    //        rewardVideoAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
    //        rewardVideoAd.OnAdClosed += HandleRewardBasedVideoClosed;
    //        rewardVideoAd.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

    //        //real AdRequest adRequest = new AdRequest.Builder().Build();
    //        AdRequest adRequest = new AdRequest.Builder().AddTestDevice("A99580108C35C670BFFC673C52DE7EB1").Build();//test
    //        rewardVideoAd.LoadAd(adRequest, video_ID);
    //    }
    public void destroyBanner() {
        if (bannerAD != null)
        {
            bannerAD.Destroy();
            bannerAD = null;
        }
    }
    //public void Display_Banner()
    //{
    //    this.RequestBanner();
    //    //Debug.Log("banerkurwaaaaaaa");
    //}
    public void Display_InterstitialAD()
    {
        if (interstitialAD != null)
        {
            if (interstitialAD.IsLoaded())
            {
                interstitialAD.Show();
                Debug.Log("Interstitialad Show");
            }
            else
            {
                Debug.Log("Interstitialad is not ready yet");
            }
        }
        else {
            Debug.Log("Interstitialad NULL");
            CreateInterstitial();
        }
    }
    public void Display_Reward_Video()
    {
        if (rewardVideoAd.IsLoaded())
            rewardVideoAd.Show();
        else
            Debug.Log("videoad is not ready yet");
    }

    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        loadBAnner();
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        loadBAnner();
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }

    #endregion
    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLoaded event received");
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        LoadInterstitial();
        MonoBehaviour.print(
            "HandleInterstitialFailedToLoad event received with message: " + args.Message);
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialOpened event received");
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        LoadInterstitial();
        MonoBehaviour.print("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLeftApplication event received");
    }

    #endregion
    #region RewardBasedVideo callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion
}
