﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLayoutBanner : MonoBehaviour
{
    public RectTransform rTransform;
    void Start()
    {
        if (!ADManager.instance.ReturnIsPremium())
        {
            int h= ADManager.instance.ReturnSizeBanner();
            //Debug.Log("hhhhhhhhhhhhhhhhh=" + h * (Screen.dpi / 160)+"/nh="+h+"************************************************/n********************************");
            rTransform.anchoredPosition = new Vector2(0, h* (Screen.dpi / 160));
        }

    }
}
