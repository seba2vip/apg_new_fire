﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADBanner : MonoBehaviour
{
    private bool ishHidden = false;
    private ADManager aDManager;
    void Start()
    {
        aDManager = ADManager.instance;

        if (!aDManager.ReturnIsPremium())
        {
            if (ishHidden == false)
            {
                aDManager.RequestBanner();
            }
            ishHidden = true;
            return;

        }
        else
        {

            aDManager.destroyBanner();
           
           
        }



    }
}
